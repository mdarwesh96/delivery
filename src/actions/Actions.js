import {
  LOGIN_USER_FAIL,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_ATTEMPT,
  EDIT_DELIVERY_PROFILE_SUCCESS,
  EDIT_DELIVERY_PROFILE_FAIL,
  EDIT_DELIVERY_PROFILE_AATEMPT,
  REGISTER_SUCCESS,
  REGISTER_FAILED,
  REGISTER_ATTEMPT,
  FORGET_FIRST_SUCCESS,
  FORGET_FIRST_FAIL,
  FORGET_SECOND_ATTEMPT,
  FORGET_FIRST_ATTEMPT,
  FORGET_SECOND_SUCCESS,
  FORGET_SECOND_FAIL,
  LOGOUT_USER_ATTEMPT,
  LOGOUT_USER_SUCCESS,
  LOGOUT_USER_FAILL,
  GET_DELIVERY_ORDERS_SUCCESS,
  GET_DELIVERY_ORDERS_FAIL,
  UPDATE_DELIVERY_ORDERS_SUCCESS,
  UPDATE_DELIVERY_ORDERS_FAIL,
  CANCEL_DELIVERY_ORDER_SUCCESS,
  CANCEL_DELIVERY_ORDER_FAIL,
  CONFIRM_DELIVERY_ORDER_SUCCESS,
  CONFIRM_DELIVERY_ORDER_FAIL,
  UPDATE_DELIVERY_ORDER_ATTEMPT,
  UPDATE_DELIVERY_ORDER_FAIL,
  UPDATE_DELIVERY_ORDER_SUCCESS,
  ACCEPT_DELIVERY_ORDERS_Attempt,
  ACCEPT_DELIVERY_ORDERS_SUCCESS,
  ACCEPT_DELIVERY_ORDERS_FAIL,
  GET_DELIVERY_ORDERS_Attempt,
  COUNTRIES_SUCCESS,
  COUNTRIES_FAIL,
} from './types';

// import https from 'https';

import axios from 'axios';

export const GetDeliveryOrdersAction = (id, currencyid, token) => {
  let URL =
    'http://ecommerce.waddeely.com/api/delivary/get-order/' + currencyid;

  const AuthStr = 'Bearer '.concat(token);

  return (dispatch) => {
    axios
      .get(URL, {
        headers: { Authorization: AuthStr, 'Content-Type': 'application/json' },
      })
      .then((response) => {
        try {
          dispatch({
            type: GET_DELIVERY_ORDERS_SUCCESS,
            payload: response.data,
          });
        } catch (e) {
          dispatch({ type: GET_DELIVERY_ORDERS_FAIL, error: e });
        }
      })
      .catch((error) => {
        dispatch({ type: GET_DELIVERY_ORDERS_FAIL, error: error });
      });
  };
};

export const UpdateDeliveryOrdersAction = (
  id,
  currencyid,
  token,
  updatestate
) => {
  let URL =
    'http://ecommerce.waddeely.com/api/api/delivary/request-status/' +
    id +
    '/' +
    currencyid;

  const AuthStr = 'Bearer '.concat(token);

  return (dispatch) => {
    var formData = new FormData();
    formData.append('status', updatestate);

    axios
      .post(URL, formData, {
        headers: {
          Authorization: AuthStr,
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
      .then((response) => {
        try {
          dispatch({
            type: UPDATE_DELIVERY_ORDERS_SUCCESS,
            payload: response.data,
          });
        } catch (e) {
          dispatch({ type: UPDATE_DELIVERY_ORDERS_FAIL, error: e });
        }
      })
      .catch((error) => {
        dispatch({ type: UPDATE_DELIVERY_ORDERS_FAIL, error: error });
      });
  };
};

export const ConfirmDeliveryOrdersAction = (
  id,
  currencyid,
  token,
  updatestate,
  image
) => {
  let URL =
    'http://ecommerce.waddeely.com/api/api/delivary/request-status/' +
    id +
    '/' +
    currencyid;

  const AuthStr = 'Bearer '.concat(token);

  return (dispatch) => {
    var formData = new FormData();
    formData.append('status', updatestate);
    formData.append('success', image);

    axios
      .post(URL, formData, {
        headers: {
          Authorization: AuthStr,
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
      .then((response) => {
        try {
          dispatch({
            type: CONFIRM_DELIVERY_ORDER_SUCCESS,
            payload: response.data,
          });
        } catch (e) {
          dispatch({ type: CONFIRM_DELIVERY_ORDER_FAIL, error: e });
        }
      })
      .catch((error) => {
        dispatch({ type: CONFIRM_DELIVERY_ORDER_FAIL, error: error });
      });
  };
};

export const CancelDeliveryOrdersAction = (
  id,
  currencyid,
  token,
  updatestate,
  reason
) => {
  let URL =
    'http://ecommerce.waddeely.com/api/api/delivary/request-status/' +
    id +
    '/' +
    currencyid;

  const AuthStr = 'Bearer '.concat(token);

  return (dispatch) => {
    var formData = new FormData();
    formData.append('status', updatestate);
    formData.append('failed', reason);

    axios
      .post(URL, formData, {
        headers: {
          Authorization: AuthStr,
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
      .then((response) => {
        try {
          dispatch({
            type: CANCEL_DELIVERY_ORDER_SUCCESS,
            payload: response.data,
          });
        } catch (e) {
          dispatch({ type: CANCEL_DELIVERY_ORDER_FAIL, error: e });
        }
      })
      .catch((error) => {
        dispatch({ type: CANCEL_DELIVERY_ORDER_FAIL, error: error });
      });
  };
};
export const EditDeliveryAction = (
  name,
  phone,
  address,
  city,
  imagebaseprofile,
  cityid,
  token
) => {
  let URL = 'http://ecommerce.waddeely.com/api/api/update-user';
  const AuthStr = 'Bearer '.concat(token);

  return (dispatch) => {
    var formData = new FormData();
    formData.append('name', name);
    formData.append('address[0][address_name]', '' + address);
    formData.append('address[0][lat]', 222222);
    formData.append('language', 0);
    formData.append('address[0][lang]', 333333);
    formData.append('address[1][address_name]', '' + city);
    formData.append('address[1][lat]', cityid);
    formData.append('address[1][lang]', cityid);
    formData.append('phone[0][phone]', parseInt(phone));
    formData.append('photo', imagebaseprofile);
    formData.append('range', '7');

    axios
      .post(URL, formData, {
        headers: {
          Authorization: AuthStr,
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
      .then((response) => {
        try {
          dispatch({
            type: EDIT_DELIVERY_PROFILE_SUCCESS,
            payload: response.data,
          });
        } catch (e) {
          dispatch({ type: EDIT_DELIVERY_PROFILE_FAIL, error: e });
        }
      })
      .catch((error) => {
        dispatch({ type: EDIT_DELIVERY_PROFILE_FAIL, error: error });
      });
  };
};
export const GetCountriesCitiesCounranciesAction = () => {
  return (dispatch) => {
    // const agent = new https.Agent({
    //   rejectUnauthorized: false
    // });

    axios
      .get(
        'http://ecommerce.waddeely.com/api/user/get-countries',
        // , { httpsAgent: agent }
        {
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded',
            Accept: 'application/json',
            //  'Content-Type': 'application/json',
            //other header fields
          },
        }
      )

      .then((resp) => {
        dispatch({ type: COUNTRIES_SUCCESS, payload: resp.data });
      })

      .catch((error) => {
        try {
          dispatch({ type: COUNTRIES_FAIL, error });

          // handlefail(dispatch,error);
        } catch (er) {
          dispatch({ type: COUNTRIES_FAIL, error: er });
        }
      });
  };
};

export const RegisterAction = (
  email,
  password,
  name,
  phone,
  address,
  city,
  imagebaseprofile,
  imagebaseupload,
  cityid,
  subcityidd
) => {
  let URL = 'http://ecommerce.waddeely.com/api/register';

  return (dispatch) => {
    var formData = new FormData();
    formData.append('name', name);
    formData.append('email', email);
    formData.append('language', 0);
    formData.append('password_confirmation', password);
    formData.append('password', password);
    formData.append('device_id', 'llllllll');
    formData.append('address[0][address_name]', '' + address);
    formData.append('address[0][lat]', 222222);
    formData.append('address[0][lang]', 333333);
    formData.append('address[1][address_name]', '' + city);
    formData.append('address[1][lat]', cityid);
    formData.append('address[1][lang]', subcityidd);
    formData.append('photo', imagebaseprofile);
    formData.append('phone[0][phone]', phone);
    formData.append('type', 3);
    formData.append('id_deivary_photo', imagebaseprofile);
    formData.append('confirm_delivary_photo', imagebaseupload);
    formData.append('range', '7');

    dispatch({ type: REGISTER_ATTEMPT });

    axios
      .post(URL, formData, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      })
      .then((response) => {
        try {
          dispatch({ type: REGISTER_SUCCESS, payload: response.data });
        } catch (e) {
          dispatch({ type: REGISTER_FAILED, error: e });
        }
      })
      .catch((error) => {
        try {
          dispatch({
            type: REGISTER_FAILED,
            error: error.response.data.errors.email[0],
          });
        } catch (ee) {
          dispatch({
            type: REGISTER_FAILED,
            error: error.response.data.errors,
          });
        }
      });
  };
};

export const ForgetPasswordFirstAction = (email) => {
  let URL = 'http://ecommerce.waddeely.com/api/password/email';

  return (dispatch) => {
    var formData = new FormData();
    formData.append('email', email);
    dispatch({ type: FORGET_FIRST_ATTEMPT });

    axios
      .post(URL, formData, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      })
      .then((response) => {
        try {
          dispatch({ type: FORGET_FIRST_SUCCESS, payload: response.data });
        } catch (e) {
          dispatch({ type: FORGET_FIRST_FAIL, error: e });
        }
      })
      .catch((error) => {
        dispatch({ type: FORGET_FIRST_FAIL, error: error });
      });
  };
};

export const LogoutAction = (token) => {
  let URL = 'https://ecommerce.waddeely.com/api/logout';

  return (dispatch) => {
    var formData = new FormData();
    const AuthStr = 'Bearer '.concat(token);
    dispatch({ type: LOGOUT_USER_ATTEMPT });
    axios
      .post(URL, formData, {
        headers: {
          Authorization: AuthStr,
          'Content-Type': 'application/x-www-form-urlencoded',
        },
      })
      .then((response) => {
        try {
          dispatch({ type: LOGOUT_USER_SUCCESS, payload: response.data });
        } catch (e) {
          dispatch({ type: LOGOUT_USER_FAILL, error: e });
        }
      })
      .catch((error) => {
        dispatch({ type: LOGOUT_USER_FAILL, error: error });
      });
  };
};
export const ForgetPasswordSecondAction = (email, code, newpassword) => {
  let URL = 'http://ecommerce.waddeely.com/api/password/reset';

  return (dispatch) => {
    var formData = new FormData();
    formData.append('email', email);
    formData.append('password', newpassword);
    formData.append('token', code);

    formData.append('password_confirmation', newpassword);

    dispatch({ type: FORGET_SECOND_ATTEMPT });

    axios
      .post(URL, formData, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      })
      .then((response) => {
        try {
          dispatch({ type: FORGET_SECOND_SUCCESS, payload: response.data });
        } catch (e) {
          dispatch({ type: FORGET_SECOND_FAIL, error: e });
        }
      })
      .catch((error) => {
        dispatch({ type: FORGET_SECOND_FAIL, error: error });
      });
  };
};

export const LoginUserAction = (email, password, devicetoken) => {
  let URL = 'http://ecommerce.waddeely.com/api/login';

  return (dispatch) => {
    var formData = new FormData();
    formData.append('password', password);
    formData.append('email', email);
    formData.append('language', 0);
    formData.append('device_id', devicetoken);

    dispatch({ type: LOGIN_USER_ATTEMPT });
    dispatch({ type: EDIT_DELIVERY_PROFILE_AATEMPT });

    axios
      .post(URL, formData, {
        headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
      })
      .then((response) => {
        try {
          dispatch({ type: LOGIN_USER_SUCCESS, payload: response.data });
        } catch (e) {
          dispatch({ type: LOGIN_USER_FAIL, error: e });
        }
      })
      .catch((error) => {
        dispatch({ type: LOGIN_USER_FAIL, error: error });
      });
  };
};

export const UpdateOrdersAction = (
  orderid,
  token,
  orderstate,
  reason,
  success
) => {
  let URL =
    'http://ecommerce.waddeely.com/api/delivary/request-status/' + orderid;

  const AuthStr = 'Bearer '.concat(token);
  var formData = new FormData();

  formData.append('status', orderstate);

  if (reason != '' && orderstate === 10) {
    formData.append('failed', 'canceled from delivery' + reason);
  }
  if (success != '') {
    formData.append('success', success);
  }

  return (dispatch) => {
  
    dispatch({ type: UPDATE_DELIVERY_ORDER_ATTEMPT });

    axios
      .post(URL, formData, {
        headers: { Authorization: AuthStr, 'Content-Type': 'application/json' },
      })
      .then((response) => {
        
        try {
          dispatch({
            type: UPDATE_DELIVERY_ORDER_SUCCESS,
            payload: response.data,
          });
        } catch (e) {
          
          dispatch({ type: UPDATE_DELIVERY_ORDER_FAIL, error: e });
        }
      })
      .catch((error) => {
       
        dispatch({ type: UPDATE_DELIVERY_ORDER_FAIL, error: error });
      });
  };
};

export const GetOrdersAction = (currencyid, token, userid) => {
  let URL = 'http://ecommerce.waddeely.com/api/delivary/get-order/' + '5';

  const AuthStr = 'Bearer '.concat(token);
  debugger;
  return (dispatch) => {
    dispatch({ type: GET_DELIVERY_ORDERS_Attempt });
    debugger;
    axios
      .get(URL, {
        headers: { Authorization: AuthStr, 'Content-Type': 'application/json' },
      })
      .then((response) => {
     debugger;
        try {
          dispatch({
            type: GET_DELIVERY_ORDERS_SUCCESS,
            payload: response.data,
          });
        } catch (e) {
          dispatch({ type: GET_DELIVERY_ORDERS_FAIL, error: e });
        }
      })
      .catch((error) => {
       
        dispatch({ type: GET_DELIVERY_ORDERS_FAIL, error: error });
      });
  };
};
export const EmptyAcceptOrder = () => {
  return (dispatch) => {
    dispatch({ type: ACCEPT_DELIVERY_ORDERS_Attempt });
  };
};

export const AcceptOrderAction = (token, ordeid) => {
  let URL =
    'http://ecommerce.waddeely.com/api/delivary/request-order/' + ordeid;

  const AuthStr = 'Bearer '.concat(token);

  return (dispatch) => {
    dispatch({ type: ACCEPT_DELIVERY_ORDERS_Attempt });

    axios
      .get(URL, {
        headers: { Authorization: AuthStr, 'Content-Type': 'application/json' },
      })
      .then((response) => {
        try {
          dispatch({
            type: ACCEPT_DELIVERY_ORDERS_SUCCESS,
            payload: response.data,
          });
        } catch (e) {
          dispatch({ type: ACCEPT_DELIVERY_ORDERS_FAIL, error: e });
        }
      })
      .catch((error) => {
        dispatch({ type: ACCEPT_DELIVERY_ORDERS_FAIL, error: error });
      });
  };
};
