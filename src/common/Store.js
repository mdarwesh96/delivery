import {createStore, applyMiddleware} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';
import AsyncStorage from '@react-native-async-storage/async-storage';

import ReduxThunk from 'redux-thunk';
import reducers from '../reducers/reducers';

// import rootReducer from './reducers'; // the value from combineReducers

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  // stateReconciler: autoMergeLevel2 // see "Merge Process" section for details.
  // whitelist: ['auth'],
  //  blacklist: ['merchants', 'cart', 'manager']
};

const pReducer = persistReducer(persistConfig, reducers);

export const store = createStore(pReducer, {}, applyMiddleware(ReduxThunk));
export const persistor = persistStore(store);

// export default store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
