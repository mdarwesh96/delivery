import {combineReducers} from 'redux';

import {
    LOGIN_USER_FAIL,LOGIN_USER_SUCCESS,LOGIN_USER_ATTEMPT,
    EDIT_DELIVERY_PROFILE_SUCCESS,EDIT_DELIVERY_PROFILE_FAIL,EDIT_DELIVERY_PROFILE_AATEMPT,
    REGISTER_SUCCESS,REGISTER_FAILED,REGISTER_ATTEMPT,
    FORGET_FIRST_SUCCESS,FORGET_FIRST_FAIL,FORGET_SECOND_ATTEMPT,FORGET_FIRST_ATTEMPT,
    FORGET_SECOND_SUCCESS,FORGET_SECOND_FAIL,
    GET_DELIVERY_ORDERS_SUCCESS,GET_DELIVERY_ORDERS_FAIL,
    LOGOUT_USER_ATTEMPT,LOGOUT_USER_SUCCESS,LOGOUT_USER_FAILL,
    UPDATE_DELIVERY_ORDERS_SUCCESS,UPDATE_DELIVERY_ORDERS_FAIL,
    CANCEL_DELIVERY_ORDER_SUCCESS,CANCEL_DELIVERY_ORDER_FAIL
    ,CONFIRM_DELIVERY_ORDER_SUCCESS,CONFIRM_DELIVERY_ORDER_FAIL,
    UPDATE_DELIVERY_ORDER_ATTEMPT,UPDATE_DELIVERY_ORDER_FAIL,UPDATE_DELIVERY_ORDER_SUCCESS,
    GET_DELIVERY_ORDERS_Attempt,COUNTRIES_FAIL,COUNTRIES_SUCCESS,
    ACCEPT_DELIVERY_ORDERS_Attempt,ACCEPT_DELIVERY_ORDERS_SUCCESS,ACCEPT_DELIVERY_ORDERS_FAIL,

  } from '../actions/types';
  
  const INITIAL_STATE = {error: ''};

  const LogoutReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
       
      case LOGOUT_USER_ATTEMPT:
      {
     
        return {error:""};
      }
    
      case LOGOUT_USER_FAILL:
      {
     
        return {error: action.error};
      }
      case LOGOUT_USER_SUCCESS: {
  
        return {error: 'success',payload:action.payload};
      }
      default:
   
  
        return state;
    }
  };
   
 
  const LoginReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    
      case LOGIN_USER_ATTEMPT:
      {

        return {error: ''};
      }


      case LOGIN_USER_FAIL:
      {

        return {error: action.error};
      }
      case LOGIN_USER_SUCCESS: {

        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };

  const RegisterReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    case REGISTER_ATTEMPT:
    {
      return {error: ''};

    }
      case REGISTER_FAILED:
      {

        return {error: action.error};
      }
      case REGISTER_SUCCESS: {

        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };
  const ForgetPasswordSecondReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case FORGET_SECOND_ATTEMPT:
      {
        return {error: ''};
  
      }
      case FORGET_SECOND_FAIL:
      {

        return {error: action.error};
      }
      case FORGET_SECOND_SUCCESS: {
  
        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };


  const ForgetPasswordFirstReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    case FORGET_FIRST_ATTEMPT:
    {
      return {error: ''};

    }
      case FORGET_FIRST_FAIL:
      {

        return {error: action.error};
      }
      case FORGET_FIRST_SUCCESS: {

        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };

  const GetDeliveryOrdersReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
      case GET_DELIVERY_ORDERS_Attempt:
      {

        return {error:''};
      }
    
      case GET_DELIVERY_ORDERS_FAIL:
      {

        return {error: action.error};
      }
      case GET_DELIVERY_ORDERS_SUCCESS: {


        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };
  const UpdateDeliveryOrdersReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    
      case UPDATE_DELIVERY_ORDERS_FAIL:
      {

        return {error: action.error};
      }
      case UPDATE_DELIVERY_ORDERS_SUCCESS: {
  
        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };
  const ConfirmDeliveryOrdersReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    
      case CONFIRM_DELIVERY_ORDER_FAIL:
      {

        return {error: action.error};
      }
      case CONFIRM_DELIVERY_ORDER_SUCCESS: {
  
        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };
  const CancelDeliveryOrdersReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    
      case CANCEL_DELIVERY_ORDER_FAIL:
      {

        return {error: action.error};
      }
      case CANCEL_DELIVERY_ORDER_SUCCESS: {
  
        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };


  const EditDeliveryProfileReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    case EDIT_DELIVERY_PROFILE_AATEMPT:
    {
      return {error: ''};

    }
      case EDIT_DELIVERY_PROFILE_FAIL:
      {

        return {error: action.error};
      }
      case EDIT_DELIVERY_PROFILE_SUCCESS: {

        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };

  const UpdateDeliveryOrederReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    case UPDATE_DELIVERY_ORDER_ATTEMPT:
    {
      return {error: ''};

    }
      case UPDATE_DELIVERY_ORDER_FAIL:
      {

        return {error: action.error};
      }
      case UPDATE_DELIVERY_ORDER_SUCCESS: {

        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };

  const GetCountriesReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    
      case COUNTRIES_FAIL:
      {
      
        return {error: action.error};
      }
      case COUNTRIES_SUCCESS: {

        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };


  const AcceptOrderReducer = (state = INITIAL_STATE, action) => {
    switch (action.type) {
    
      case ACCEPT_DELIVERY_ORDERS_Attempt:
      {
      
        return {error: ''};
      }
      case ACCEPT_DELIVERY_ORDERS_FAIL:
      {
      
        return {error: action.error};
      }
      case ACCEPT_DELIVERY_ORDERS_SUCCESS: {

        return {error: 'success',payload:action.payload};
      }
      default:
        return state;
    }
  };

  export default combineReducers({
    LoginReducer:LoginReducer,
    RegisterReducer:RegisterReducer,
    ForgetPasswordFirstReducer:ForgetPasswordFirstReducer,
    ForgetPasswordSecondReducer:ForgetPasswordSecondReducer,
    GetDeliveryOrdersReducer:GetDeliveryOrdersReducer,
    UpdateDeliveryOrdersReducer:UpdateDeliveryOrdersReducer,
    ConfirmDeliveryOrdersReducer:ConfirmDeliveryOrdersReducer,
    CancelDeliveryOrdersReducer:CancelDeliveryOrdersReducer,
    EditDeliveryProfileReducer:EditDeliveryProfileReducer,
    UpdateDeliveryOrederReducer:UpdateDeliveryOrederReducer,
    GetCountriesReducer:GetCountriesReducer,
  AcceptOrderReducer,AcceptOrderReducer,
  LogoutReducer:LogoutReducer

});
