import React, { useEffect, useState, useRef } from 'react';
import { ImageBackground, View, Image, Text } from 'react-native';
import { ProgressBar } from 'react-native-paper';

import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-simple-toast';
import { useDispatch, useSelector } from 'react-redux';
import { GetDeliveryOrdersAction } from '../../actions/Actions';
import { LoginUserAction } from '../../actions/Actions';

let email, password, devicetoken;
export default function LoadingScreen(props) {
  const dispatch = useDispatch();

  const [selected, setselected] = useState('');
  const [progress, setprogress] = useState(0.1);

  let bg = require('../../imgs/bg.png');

  const LoginReducer = useSelector((state) => state.LoginReducer);

  if (selected === 'login') {
    if (
      typeof LoginReducer.payload !== 'undefined' &&
      typeof LoginReducer.payload.status !== 'undefined'
    ) {
      if (LoginReducer.payload.data.status === '2') {
        Toast.showWithGravity(
          'حسابك غير نشط يرجى الاتصال بالإدارة  ',
          Toast.LONG,
          Toast.BOTTOM
        );
        setselected('');

        // setInterval(() => {

        // }, 1000);
      } else if (
        typeof LoginReducer.error !== 'undefined' &&
        LoginReducer.error === 'success'
      ) {
        if ('' + LoginReducer.payload.data.type === '3') {
          AsyncStorage.setItem('loggedin', 'true').then(() => {
            AsyncStorage.setItem('email', email).then(() => {
              AsyncStorage.setItem('password', password).then(() => {
                AsyncStorage.setItem(
                  'name',
                  LoginReducer.payload.data.name
                ).then(() => {
                  AsyncStorage.setItem(
                    'token',
                    LoginReducer.payload.token.token
                  ).then(() => {
                    AsyncStorage.setItem(
                      'id',
                      '' + LoginReducer.payload.data.id
                    ).then(() => {
                      let id = LoginReducer.payload.data.id;
                      // setfirstlogin (true);

                      setselected('getorders');

                      dispatch(
                        GetDeliveryOrdersAction(
                          id,
                          5,
                          LoginReducer.payload.token.token
                        )
                      );

                      props.navigation.navigate('OrdersStack', {
                        token: LoginReducer.payload.token.token,
                      });
                    });
                  });
                });
              });
            });
          });
        } else {
          setselected('');
          Toast.showWithGravity(
            'هذا المستخدم ليس رجل توصيل',
            Toast.LONG,
            Toast.BOTTOM
          );
        }
      }
    } else {
      if (
        typeof LoginReducer.error !== 'undefined' &&
        LoginReducer.error !== 'success'
      ) {
        if (
          typeof LoginReducer.error.response !== 'undefined' &&
          typeof LoginReducer.error.response.data !== 'undefined' &&
          typeof LoginReducer.error.response.data.message !== 'undefined' &&
          LoginReducer.error.response.data.message === 'password is wrong'
        ) {
          Toast.showWithGravity('كلمة السر خاطئة', Toast.SHORT, Toast.BOTTOM);

          AsyncStorage.setItem('loggedin', 'false').then(() => {
            AsyncStorage.setItem('email', 'null').then(() => {
              AsyncStorage.setItem('name', 'null').then(() => {
                AsyncStorage.setItem('password', 'null').then(() => {
                  AsyncStorage.setItem('token', 'null').then(() => {
                    AsyncStorage.setItem('id', 'null').then(() => {
                      setselected('');

                      // }, 1000);
                    });
                  });
                });
              });
            });
          });
        } else if (
          typeof LoginReducer.error.response !== 'undefined' &&
          typeof LoginReducer.error.response.data !== 'undefined' &&
          typeof LoginReducer.error.response.data.message !== 'undefined'
        ) {
          Toast.showWithGravity(
            'بريد إلكتروني خاطئ',
            Toast.SHORT,
            Toast.BOTTOM
          );

          AsyncStorage.setItem('loggedin', 'false').then(() => {
            AsyncStorage.setItem('email', 'null').then(() => {
              AsyncStorage.setItem('password', 'null').then(() => {
                AsyncStorage.setItem('token', 'null').then(() => {
                  AsyncStorage.setItem('id', 'null').then(() => {
                    setselected('');

                    // }, 1000);
                  });
                });
              });
            });
          });
        } else if (LoginReducer.error !== '') {
          setselected('');
        }
      } else {
        //   setselectedd('')
        //   setInterval(() => {
        //     setspinner(false)
        //   }, 1000);
      }
    }
  }

  const loadApp = async () => {
    try {
      email = await AsyncStorage.getItem('email');

      password = await AsyncStorage.getItem('password');

      setselected('login');

      devicetoken = await AsyncStorage.getItem('devicetoken');

      if (devicetoken === null) {
        devicetoken = 'null';
      }
      dispatch(LoginUserAction(email.trim(), password, devicetoken));
    } catch (e) {}
  };
  useEffect(() => {
    loadApp();
  }, []);
  useEffect(() => {
    timeoutHandle = setTimeout(() => {
      let newp = parseFloat(progress + progress);
      setprogress(newp);
    }, 100);
  }, [progress]);

  return (
    <ImageBackground
      style={{
        height: '100%',
        width: '100%',
      }}
      source={bg}>
      <View
        style={{
          flexDirection: 'column',
          marginVertical: '10%',
          width: '100%',
          justifyContent: 'center',

          height: '80%',
        }}>
        <Image
          style={{
            width: '100%',
            height: 150,
          }}
          resizeMode='contain'
          source={require('../../imgs/logoooo.png')}></Image>
   <View style={{width:'100%',justifyContent:'center',alignItems:'center'}}>
        <Image
                 source={require('../../imgs/headerlogo.png')}
              style={{tintColor:'white',width:'30%',height:20, marginTop: -10}}
              resizeMode='stretch'
                />
                </View>
        <ProgressBar progress={progress} style={{ margin: 40 }} />
      </View>
    </ImageBackground>
  );
}
