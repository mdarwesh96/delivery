import React, { useEffect, useState, useRef } from 'react';
import {
  View,
  StyleSheet,
  SafeAreaView,
  Text,
  TextInput,
  Animated,
  Platform,
  Dimensions,
  ImageBackground,
  Image,
  TouchableHighlight,
  Keyboard,
} from 'react-native';
let enn = true;
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Icon from 'react-native-vector-icons/Ionicons';
import ImagePicker from 'react-native-image-crop-picker';
import ImgToBase64 from 'react-native-image-base64';
import Icond from 'react-native-vector-icons/FontAwesome5';
import Spinner from 'react-native-loading-spinner-overlay';
import BottomSheet from 'reanimated-bottom-sheet';
import ActionSheet from 'react-native-actions-sheet';
import LinearGradient from 'react-native-linear-gradient';
import { showMessage, hideMessage } from "react-native-flash-message";
const DEVICE_width = Dimensions.get('window').width;
const DEVICE_hight = Dimensions.get('window').height;
const MARGIN = 60;
import SwipeablePanel from '../../common/actualComponents/Panel';
import SelectableFlatlist, { STATE } from 'react-native-selectable-flatlist';
import { ScrollView } from 'react-native-gesture-handler';
import { CheckBox } from 'react-native-elements';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-simple-toast';
import { useDispatch, useSelector } from 'react-redux';
import { GetDeliveryOrdersAction } from '../../actions/Actions';
const screenHeight = Dimensions.get('window').height;
import { LoginUserAction } from '../../actions/Actions';
import { RegisterAction } from '../../actions/Actions';
import { ForgetPasswordFirstAction } from '../../actions/Actions';
import { ForgetPasswordSecondAction } from '../../actions/Actions';
let opened = false;
export default function LoginScreen(props) {
  const dispatch = useDispatch();
  const sheetRef = useRef(null);
  const [sheetIsOpen, setSheetIsOpen] = useState(false);
  const [imageIsProfile, setimageIsProfile] = useState(false);
  const [srcimage, setsrcimage] = useState(require('../../imgs/identity.jpg'));
  let header = require('../../imgs/registerheader.png');
  const [srcuploadimage, setsrcuploadimage] = useState(
    require('../../imgs/defaultt.png')
  );
  const [count, setcount] = useState(0);
  const [newpassword, setnewpassword] = useState('');
  const [selected, setselected] = useState('');
  const [email, setemail] = useState('');
  const [forgetemail, setforgetemail] = useState('');
  const [password, setpassword] = useState('');
  const [name, setname] = useState('');
  const [code, setcode] = useState('');
  const [phone, setphone] = useState('');
  const [imagebaseprofile, setimagebaseprofile] = useState('');
  const [imagebaseupload, setimagebaseupload] = useState('');
  const [clickable, setclickable] = useState(false);
  const [swipeablePanelterms, setswipeablePanelterms] = useState(false);
  const [devicetoken, setdevicetoken] = useState('dddddd');

  const [checked, setchecked] = useState(false);
  const [address, setaddress] = useState('');
  const [spinner, setspinner] = useState(false);
  const [currencyid, setcurrencyid] = useState('1');
  const [currency, setcurrency] = useState('درهم اماراتي');
  const [citylist, setcitylist] = useState([]);
  const [subcitylist, setsubcitylist] = useState([]);
  const [cityid, setcityid] = useState(null);
  const [subcityid, setsubcityid] = useState(null);
  const [subcityidd, setsubcityidd] = useState(null);
  const [country, setcountry] = useState('الإمارات العربية المتحدة');
  const [fullheighttt, setfullheighttt] = useState(0);
  const [subcityselected, setsubcityselected] = useState('المنطقة');
  const [cityselected, setcityselected] = useState('المدينة');
  const [subcityswipable, setsubcityswipable] = useState(false);
  const [cityswipable, setcityswipable] = useState(false);
  const [terms,setterms]=useState([require('../../imgs/delivery1.png'),require('../../imgs/delivery2.png'),require('../../imgs/delivery3.png'),
  require('../../imgs/delivery4.png'),require('../../imgs/delivery5.png'),require('../../imgs/delivery6.png'),
  require('../../imgs/delivery7.png'),require('../../imgs/delivery8.png'),require('../../imgs/delivery9.png'),require('../../imgs/delivery10.png')])

  const [animation, setanimation] = useState(new Animated.Value(0));
  //name:'',,
  //   }

  const LoginReducer = useSelector((state) => state.LoginReducer);
  const RegisterReducer = useSelector((state) => state.RegisterReducer);
  const ForgetPasswordFirstReducer = useSelector(
    (state) => state.ForgetPasswordFirstReducer
  );
  const ForgetPasswordSecondReducer = useSelector(
    (state) => state.ForgetPasswordSecondReducer
  );
  const GetDeliveryOrdersReducer = useSelector(
    (state) => state.GetDeliveryOrdersReducer
  );
  const GetCountriesReducer = useSelector((state) => state.GetCountriesReducer);

  const actionSheetRef = useRef();
  const showModalVisible = () => {
    actionSheetRef.current?.setModalVisible(true);
  };
  const hideModalVisible = () => {
    actionSheetRef.current?.setModalVisible(false);
  };

  actionSheetRef.current?.snapToOffset(300);

  // function handleBackButtonClick() {

  //   setfullheighttt(0);

  // opened=false
  //   return true;
  // }

  if (enn) {
    try {
      let citylist = [];

      if (
        typeof GetCountriesReducer !== 'undefined' &&
        typeof GetCountriesReducer.payload !== 'undefined' &&
        typeof GetCountriesReducer.payload.data !== 'undefined'
      ) {
        for (let f = 0; f < GetCountriesReducer.payload.data.length; f++) {
          if (GetCountriesReducer.payload.data[f].country_name_ar === country) {
            if (GetCountriesReducer.payload.data[f].cities.length > 0) {
              for (
                let d = 0;
                d < GetCountriesReducer.payload.data[f].cities.length;
                d++
              ) {
                citylist.push({
                  id: d + 1,
                  name: GetCountriesReducer.payload.data[f].cities[d]
                    .city_name_ar,
                  idd: GetCountriesReducer.payload.data[f].cities[d].id,
                });
              }
            }
          }
          setcitylist(citylist);
          enn = false;
        }
      } else {
        setcitylist([]);
        enn = false;
      }
    } catch (e) {
      setcitylist([]);
      enn = false;
    }
  }

  closepanell = (index) => {
    setfullheighttt(0);
    setsubcityswipable(false);
    // setcountryswipable(false)
    setcityswipable(false);

    setnewpassword('');
    setcode('');
    setforgetemail('');
    setcount(0);
  };
  const getsubcitylist = (country, cityid) => {
    let subcitylist = [];

    for (let f = 0; f < GetCountriesReducer.payload.data.length; f++) {
      if (GetCountriesReducer.payload.data[f].country_name_ar === country) {
        if (GetCountriesReducer.payload.data[f].subcities.length > 0) {
          for (
            let d = 0;
            d < GetCountriesReducer.payload.data[f].subcities.length;
            d++
          ) {
            if (
              GetCountriesReducer.payload.data[f].subcities[d].city_id ===
              cityid
            ) {
              subcitylist.push({
                id: d + 1,
                name: GetCountriesReducer.payload.data[f].subcities[d]
                  .subcity_name_ar,
                idd: GetCountriesReducer.payload.data[f].subcities[d].id,
              });
            }
          }
        }
      }
    }

    return subcitylist;
  };
  const getinitial = (id) => {
    if (id === null) {
      return id - 1;
    } else return 1;
  };
  const handleOpen = () => {
    setfullheighttt(0);
    sheetRef.current.snapTo(sheetIsOpen ? fullheighttt : 0);

    opened = false;

    setforgetemail('');
    setcount(0);
    setcode('');
    setnewpassword('');

    Animated.timing(animation, {
      toValue: 1,
      duration: 300,
      useNativeDriver: true,
    }).start();
    setclickable(true);
    setemail('');
    setpassword('');
    setselected('');
    setcityswipable(false);

    setsubcityswipable(false);
  };
  const handleClose = () => {
    setfullheighttt(0);

    opened = false;

    Animated.timing(animation, {
      toValue: 0,
      duration: 200,
      useNativeDriver: true,
    }).start();
    setname('');
    setpassword('');
    setemail('');
    setforgetemail('');
    setclickable(false);
    setphone('');
    setimagebaseprofile('');
    setimagebaseupload('');
    setaddress('');
    setsrcimage(require('../../imgs/identity.jpg'));
    setsrcuploadimage(require('../../imgs/defaultt.png'));
    setchecked(false);
    setcityid(null);
    setsubcityid(null);
    setsubcityidd(null);
    setcityswipable(false);

    setsubcityswipable(false);
    setnewpassword('');
    setcode('');
    setforgetemail('');
    setcount(0);
    // setcountryid(null)

    setsubcityswipable(false);

    // setcountryswipable(false)
    setcityswipable(false);

    // setcountry('الدولة')
    setsubcityselected('المنطقة');
    setcityselected('المدينة');
  };

  const renderContent = () => (
    <View
      style={{
        flexDirection: 'column',
        height: DEVICE_hight / 3,
        backgroundColor: 'white',
      }}
      resizeMode='stretch'>
      <View
        style={{
          flexDirection: 'column',
          padding: 20,
        }}>
        {count === 0 && (
          <Text
            style={{
              textAlign: 'right',
              margin: 10,
              fontSize: 17,
              color: '#0a2c6b',
            }}>
            اكتب بريدك الإلكتروني
          </Text>
        )}
        {count === 0 && (
          <View style={styles.inputWrapper}>
            <TextInput
              style={styles.input}
              placeholder={'البريد الإلكتروني'}
              autoCapitalize={'none'}
              // autoFocus={fff}
              returnKeyType={'done'}
              autoCorrect={false}
              onChangeText={(email) => {
                setforgetemail(email);
                // or some other action

                // setKeyboardVisible(true);
              }}
              value={forgetemail}
              secureTextEntry={false}
              placeholderTextColor='#3c9bc6'
              underlineColorAndroid='transparent'
            />
          </View>
        )}
        {count === 1 && (
          <View>
            <View style={{ height: 20 }} />
            <View style={styles.inputWrapper}>
              <TextInput
                style={styles.input}
                placeholder={' رمز التأكيد'}
                autoCapitalize={'none'}
                returnKeyType={'done'}
                autoCorrect={false}
                onChangeText={(code) => {
                  setcode(code);
                }}
                value={code}
                secureTextEntry={false}
                placeholderTextColor='#3c9bc6'
                underlineColorAndroid='transparent'
              />
            </View>
            <View style={styles.inputWrapper}>
              <TextInput
                secureTextEntry={true}
                placeholder={'كلمة المرور الجديدة '}
                returnKeyType={'done'}
                autoCapitalize={'none'}
                autoCorrect={false}
                secureTextEntry
                onChangeText={(newpassword) => {
                  setnewpassword(newpassword);
                }}
                value={newpassword}
                style={styles.input}
                placeholderTextColor='#3c9bc6'
                underlineColorAndroid='transparent'
              />
            </View>
          </View>
        )}
        <TouchableHighlight
          underlayColor='transparent'
          activeOpacity={0.2}
          style={{
            alignItems: 'center',
            marginTop: 20,
          }}
          onPress={(_) => {
            if (count === 0) {
              if (forgetemail.trim().length > 0) {
                if (validateEmail(forgetemail.trim())) {
                  setselected('forgetpasswordfirst');

                  dispatch(ForgetPasswordFirstAction(forgetemail.trim()));

                  // setInterval(() => {

                  setspinner(true);

                  // }, 1000);
                } else {
                  Toast.showWithGravity(
                    'بريد إلكتروني خاطئ',
                    Toast.LONG,
                    Toast.BOTTOM
                  );
                }
              } else {
                Toast.showWithGravity(
                  'تحتاج إلى إضافة الحقول المفقودة',
                  Toast.LONG,
                  Toast.BOTTOM
                );
              }
            } else if (count === 1) {
              if (code.length > 0 && newpassword.length > 0) {
                if (newpassword.length >= 8) {
                  dispatch(
                    ForgetPasswordSecondAction(forgetemail, code, newpassword)
                  );

                  setspinner(true);

                  setselected('forgetpasswordsecond');
                } else {
                  Toast.showWithGravity(
                    'طول كلمة المرور غير صالحة',
                    Toast.LONG,
                    Toast.BOTTOM
                  );
                }
              } else {
                Toast.showWithGravity(
                  'تحتاج إلى إضافة الحقول المفقودة',
                  Toast.LONG,
                  Toast.BOTTOM
                );
              }
            }
          }}>
          <LinearGradient
            colors={['#04b5eb', '#06d4e5', '#06d4e5']}
            style={{ borderRadius: 20 }}>
            <Text
              style={{
                color: 'white',
                width: '50%',
                marginHorizontal: '25%',
                marginVertical: 8,
                fontSize: 20,
              }}>
              إرسال
            </Text>
          </LinearGradient>
        </TouchableHighlight>
      </View>
    </View>
  );

  const itemsSelectedcity = (selectedItem) => {
    setcityid(selectedItem.id);
    setsubcityid(null);
    setsubcityidd(null);

    //  setcountryswipable(false)
    setcityswipable(false);
    setcityselected(selectedItem.name);
    setsubcityselected('المنطقة');
    setsubcitylist(getsubcitylist(country, selectedItem.idd));
  };
  const itemsSelectedsubcity = (selectedItem) => {
    setsubcityid(selectedItem.id);
    setsubcityidd(selectedItem.idd);

    //  setcountryswipable(false)
    setcityswipable(false);
    setsubcityswipable(false);
    setsubcityselected(selectedItem.name);
  };

  const renderHeader = () => {
    return (
      <View style={styles.header}>
        <View style={styles.panelHandle} />
      </View>
    );
  };

  const rowcityItem = (item) => (
    <TouchableHighlight
    underlayColor='transparent'
    activeOpacity={0.2}
      style={{
        flex: 1,
        borderBottomWidth: 0.0,
        alignItems: 'stretch',
        paddingLeft: 30,
        paddingRight: 30,
        justifyContent: 'space-between',
        paddingVertical: 5,
        flexDirection: 'row-reverse',
        width: '100%',
        borderBottomColor: 'white',
      }}
      onPress={(_) => {
        setcityid(item.id);
        setsubcityid(null);
        setsubcityidd(null);

        // setcountryswipable(false)
        setcityswipable(false);
        setcityselected(item.name);
        setsubcityselected('المنطقة');
        setsubcitylist(getsubcitylist(country, item.idd));
      }}>
      <View style={{ width: '120%' }}>
        <Text style={{ fontSize: 18, color: '#3c9bc6', textAlign: 'right' }}>
          {item.name}
        </Text>
        <View
          style={{
            backgroundColor: '#CCCCCC',
            height: 1,
            width: '100%',
            marginTop: 8,
          }}></View>
      </View>
    </TouchableHighlight>
  );
  const rowsubcityItem = (item) => (
    <TouchableHighlight
          underlayColor='transparent'
          activeOpacity={0.2}
      style={{
        flex: 1,
        borderBottomWidth: 0.0,
        alignItems: 'stretch',
        paddingLeft: 30,
        paddingRight: 30,
        justifyContent: 'space-between',
        paddingVertical: 5,
        flexDirection: 'row-reverse',
        width: '100%',
        borderBottomColor: 'white',
      }}
      onPress={(_) => {
        setsubcityid(item.id);
        setsubcityidd(item.idd);

        // setcountryswipable(false)
        setcityswipable(false);
        setsubcityswipable(false);
        setsubcityselected(item.name);
      }}>
      <View style={{ width: '120%' }}>
        <Text style={{ fontSize: 18, color: '#3c9bc6', textAlign: 'right' }}>
          {item.name}
        </Text>
        <View
          style={{
            backgroundColor: '#CCCCCC',
            height: 1,
            width: '100%',
            marginTop: 8,
          }}></View>
      </View>
    </TouchableHighlight>
  );

  //  const itemsSelectedcounty = (selectedItem) => {

  //   //  setcountryswipable(false)
  //   //  setcountry(item.name)
  //   //  setcountryid(selectedItem.id)
  //    setcityid(null)
  //    setsubcityid(null)
  //    setsubcityidd(null)

  //    setcityselected('المدينة')
  //    setsubcityselected('المنطقة')
  //   setcitylist(getcitylist(selectedItem.name))
  //   setsubcitylist([])

  //  }

  if (selected === 'login') {
    if (
      typeof LoginReducer.payload !== 'undefined' &&
      typeof LoginReducer.payload.status !== 'undefined'
    ) {
      if (LoginReducer.payload.data.status === '2') {
        Toast.showWithGravity(
          'حسابك غير نشط يرجى الاتصال بالإدارة  ',
          Toast.LONG,
          Toast.BOTTOM
        );
        setselected('');

        // setInterval(() => {

        if (Platform.OS === 'ios') {
          setInterval(() => {
            setspinner(false);
          }, 1000);
        } else {
          // setInterval(() => {

          setspinner(false);

          // }, 1000);
        }

        // }, 1000);
      } else if (
        typeof LoginReducer.error !== 'undefined' &&
        LoginReducer.error === 'success'
      ) {
        if ('' + LoginReducer.payload.data.type === '3') {
          if (clickable) {
            AsyncStorage.setItem('loggedin', 'true').then(() => {
              AsyncStorage.setItem('email', email).then(() => {
                AsyncStorage.setItem('password', password).then(() => {
                  AsyncStorage.setItem(
                    'name',
                    LoginReducer.payload.data.name
                  ).then(() => {
                    AsyncStorage.setItem(
                      'token',
                      LoginReducer.payload.token.token
                    ).then(() => {
                      AsyncStorage.setItem(
                        'id',
                        '' + LoginReducer.payload.data.id
                      ).then(() => {
                        let id = LoginReducer.payload.data.id;
                        // setfirstlogin (true);

                        setselected('getorders');
                        setclickable(false);

                        dispatch(
                          GetDeliveryOrdersAction(
                            id,
                            currencyid,
                            LoginReducer.payload.token.token
                          )
                        );

                        // props.navigation.navigate('HomeStack', {
                        //   token:LoginReducer.payload.token.token
                        // });
                      });
                    });
                  });
                });
              });
            });
          }
        } else {
          // setInterval(() => {

          if (Platform.OS === 'ios') {
            setInterval(() => {
              setspinner(false);
            }, 1000);
          } else {
            // setInterval(() => {

            setspinner(false);

            // }, 1000);
          }

          // }, 1000);
          setemail('');
          setpassword('');
          setselected('');
          Toast.showWithGravity(
            'هذا المستخدم ليس رجل توصيل',
            Toast.LONG,
            Toast.BOTTOM
          );
        }
      }
    } else {
      if (
        typeof LoginReducer.error !== 'undefined' &&
        LoginReducer.error !== 'success'
      ) {
        if (
          typeof LoginReducer.error.response !== 'undefined' &&
          typeof LoginReducer.error.response.data !== 'undefined' &&
          typeof LoginReducer.error.response.data.message !== 'undefined' &&
          LoginReducer.error.response.data.message === 'password is wrong'
        ) {
          Toast.showWithGravity('كلمة السر خاطئة', Toast.SHORT, Toast.BOTTOM);

          AsyncStorage.setItem('loggedin', 'false').then(() => {
            AsyncStorage.setItem('email', 'null').then(() => {
              AsyncStorage.setItem('name', 'null').then(() => {
                AsyncStorage.setItem('password', 'null').then(() => {
                  AsyncStorage.setItem('token', 'null').then(() => {
                    AsyncStorage.setItem('id', 'null').then(() => {
                      setselected('');
                      setemail('');
                      setpassword('');
                      //  setInterval(() => {

                      if (Platform.OS === 'ios') {
                        setInterval(() => {
                          setspinner(false);
                        }, 1000);
                      } else {
                        // setInterval(() => {

                        setspinner(false);

                        // }, 1000);
                      }

                      // }, 1000);
                    });
                  });
                });
              });
            });
          });
        } else if (
          typeof LoginReducer.error.response !== 'undefined' &&
          typeof LoginReducer.error.response.data !== 'undefined' &&
          typeof LoginReducer.error.response.data.message !== 'undefined'
        ) {
          Toast.showWithGravity(
            'بريد إلكتروني خاطئ',
            Toast.SHORT,
            Toast.BOTTOM
          );

          AsyncStorage.setItem('loggedin', 'false').then(() => {
            AsyncStorage.setItem('email', 'null').then(() => {
              AsyncStorage.setItem('password', 'null').then(() => {
                AsyncStorage.setItem('token', 'null').then(() => {
                  AsyncStorage.setItem('id', 'null').then(() => {
                    setemail('');
                    setpassword('');
                    setselected('');

                    //  setInterval(() => {

                    if (Platform.OS === 'ios') {
                      setInterval(() => {
                        setspinner(false);
                      }, 1000);
                    } else {
                      // setInterval(() => {

                      setspinner(false);

                      // }, 1000);
                    }

                    // }, 1000);
                  });
                });
              });
            });
          });
        } else if (LoginReducer.error !== '') {
          setselected('');

          // setInterval(() => {

          if (Platform.OS === 'ios') {
            setInterval(() => {
              setspinner(false);
            }, 1000);
          } else {
            // setInterval(() => {

            if (Platform.OS === 'ios') {
              setInterval(() => {
                setspinner(false);
              }, 1000);
            } else {
              // setInterval(() => {

              setspinner(false);

              // }, 1000);
            }

            // }, 1000);
          }

          // }, 1000);

          // Toast.showWithGravity(LoginReducer.error.message, Toast.SHORT, Toast.BOTTOM)
        } else {
          //   setselectedd('')
          //   setInterval(() => {
          //     setspinner(false)
          //   }, 1000);
        }
      }
    }
  } else if (selected === 'register') {
    if (
      typeof RegisterReducer.payload !== 'undefined' &&
      RegisterReducer.error === 'success'
    ) {
      setselected('');
      // setInterval(() => {

      if (Platform.OS === 'ios') {
        setInterval(() => {
          setspinner(false);
        }, 1000);
      } else {
        // setInterval(() => {

        setspinner(false);

        // }, 1000);
      }

      // }, 1000);
      showMessage({
        message: 'لقد قمت بالتسجيل بنجاح وسنوافق على حسابك قريبًا ',
        type: "info",
        duration:2000
      });
      // Toast.showWithGravity(
      //   'لقد قمت بالتسجيل بنجاح وسنوافق على حسابك قريبًا ',
      //   Toast.LONG,
      //   Toast.BOTTOM
      // );
      handleClose();
    } else {
      if (
        typeof RegisterReducer.error !== 'undefined' &&
        RegisterReducer.error === 'The email has already been taken.'
      ) {
        // setInterval(() => {

        if (Platform.OS === 'ios') {
          setInterval(() => {
            setspinner(false);
          }, 1000);
        } else {
          // setInterval(() => {

          setspinner(false);

          // }, 1000);
        }

        // }, 1000);
        setselected('');
        Toast.showWithGravity(
          'تم بالفعل تسجيل البريد الإلكتروني من قبل ',
          Toast.LONG,
          Toast.BOTTOM
        );
      } else {
        if (RegisterReducer.error !== '') {
          // setInterval(() => {

          if (Platform.OS === 'ios') {
            setInterval(() => {
              setspinner(false);
            }, 1000);
          } else {
            // setInterval(() => {

            setspinner(false);

            // }, 1000);
          }

          // }, 1000);

          setselected('');
          Toast.showWithGravity(
            RegisterReducer.error,
            Toast.LONG,
            Toast.BOTTOM
          );
        }
      }
    }
  } else if (selected === 'getorders') {
    if (GetDeliveryOrdersReducer.error !== '') {
      AsyncStorage.setItem('loggedin', 'true').then(() => {
        if (Platform.OS === 'ios') {
          setInterval(() => {
            setspinner(false);
          }, 1000);
        } else {
          // setInterval(() => {

          setspinner(false);

          // }, 1000);
        }
        setselected('');
        props.navigation.navigate('OrdersScreen', {
          currencyid: currencyid,
          currency: currency,
        });
      });
    }
  } else if (selected === 'forgetpasswordfirst') {
    if (
      typeof ForgetPasswordFirstReducer !== 'undefined' &&
      typeof ForgetPasswordFirstReducer.error !== 'undefined' &&
      ForgetPasswordFirstReducer.error === 'success'
    ) {
      if (ForgetPasswordFirstReducer.error === 'success') {
        if (
          ForgetPasswordFirstReducer.payload.message !== 'this email not found'
        ) {
          setcount(1);
          // setInterval(() => {

          if (Platform.OS === 'ios') {
            setInterval(() => {
              setspinner(false);
            }, 1000);
          } else {
            // setInterval(() => {

            setspinner(false);

            // }, 1000);
          }

          // }, 1000);

          setselected('');

          Toast.showWithGravity(
            'يرجى التحقق من بريدك ونسخ الرمز المرسل ',
            Toast.LONG,
            Toast.BOTTOM
          );
        } else {
          // setInterval(() => {

          if (Platform.OS === 'ios') {
            setInterval(() => {
              setspinner(false);
            }, 1000);
          } else {
            // setInterval(() => {

            setspinner(false);

            // }, 1000);
          }

          // }, 1000);

          setselected('');
          Toast.showWithGravity(
            'لم يتم تسجيل هذا البريد الإلكتروني من قبل ',
            Toast.LONG,
            Toast.BOTTOM
          );
        }
      } else {
        // setInterval(() => {

        if (Platform.OS === 'ios') {
          setInterval(() => {
            setspinner(false);
          }, 1000);
        } else {
          // setInterval(() => {

          setspinner(false);

          // }, 1000);
        }

        // }, 1000);

        setselected('');
        Toast.showWithGravity(
          'لم يتم تسجيل هذا البريد الإلكتروني من قبل ',
          Toast.LONG,
          Toast.BOTTOM
        );
      }
    } else if (
      typeof ForgetPasswordFirstReducer !== 'undefined' &&
      typeof ForgetPasswordFirstReducer.error !== 'undefined' &&
      ForgetPasswordFirstReducer.error !== '' &&
      ForgetPasswordFirstReducer.error !== 'success'
    ) {
      // setInterval(() => {

      if (Platform.OS === 'ios') {
        setInterval(() => {
          setspinner(false);
        }, 1000);
      } else {
        // setInterval(() => {

        setspinner(false);

        // }, 1000);
      }

      // }, 1000);
      setselected('');
      Toast.showWithGravity('حدث خطأ عام ', Toast.LONG, Toast.BOTTOM);
    }
  } else if (selected === 'forgetpasswordsecond') {
    if (
      typeof ForgetPasswordSecondReducer !== 'undefined' &&
      typeof ForgetPasswordSecondReducer.error !== 'undefined' &&
      ForgetPasswordSecondReducer.error === 'success'
    ) {
      if (
        ForgetPasswordSecondReducer.payload.message ===
        'Password has been successfully changed'
      ) {
        setfullheighttt(0);
        setnewpassword('');
        setcode('');
        setforgetemail('');
        setcount(0);
        if (Platform.OS === 'ios') {
          setInterval(() => {
            if (Platform.OS === 'ios') {
              setInterval(() => {
                setspinner(false);
              }, 1000);
            } else {
              // setInterval(() => {

              setspinner(false);

              // }, 1000);
            }
          }, 1000);
        } else {
          // setInterval(() => {

          if (Platform.OS === 'ios') {
            setInterval(() => {
              setspinner(false);
            }, 1000);
          } else {
            // setInterval(() => {

            setspinner(false);

            // }, 1000);
          }

          // }, 1000);
        }

        setselected('');
        Toast.showWithGravity(
          'تم تغيير كلمة المرور بنجاح',
          Toast.LONG,
          Toast.BOTTOM
        );
      } else if (ForgetPasswordSecondReducer.error !== 'success') {
        // setInterval(() => {

        if (Platform.OS === 'ios') {
          setInterval(() => {
            setspinner(false);
          }, 1000);
        } else {
          // setInterval(() => {

          setspinner(false);

          // }, 1000);
        }

        // }, 1000);
        setselected('');
        Toast.showWithGravity(
          'الكود الذي تم إدخاله خاطئ  ',
          Toast.LONG,
          Toast.BOTTOM
        );
      } else {
        // setInterval(() => {

        if (Platform.OS === 'ios') {
          setInterval(() => {
            setspinner(false);
          }, 1000);
        } else {
          // setInterval(() => {

          setspinner(false);

          // }, 1000);
        }

        // }, 1000);
        setselected('');
        Toast.showWithGravity('حدث خطأ عام ', Toast.LONG, Toast.BOTTOM);
      }
    } else if (
      typeof ForgetPasswordSecondReducer !== 'undefined' &&
      typeof ForgetPasswordSecondReducer.error !== 'undefined' &&
      ForgetPasswordSecondReducer.error !== '' &&
      ForgetPasswordSecondReducer.error !== 'success'
    ) {
      // setInterval(() => {

      if (Platform.OS === 'ios') {
        setInterval(() => {
          setspinner(false);
        }, 1000);
      } else {
        // setInterval(() => {

        setspinner(false);

        // }, 1000);
      }

      // }, 1000);
      setselected('');
      Toast.showWithGravity(
        'الكود الذي تم إدخاله خاطئ  ',
        Toast.LONG,
        Toast.BOTTOM
      );
    }
  }
  const backdrop = {
    transform: [
      {
        translateY: animation.interpolate({
          inputRange: [0, 0.01],
          outputRange: [screenHeight, 0],
          extrapolate: 'clamp',
        }),
      },
    ],
    opacity: animation.interpolate({
      inputRange: [0.01, 0.5],
      outputRange: [0, 1],
      extrapolate: 'clamp',
    }),
  };

  const slideUp = {
    transform: [
      {
        translateY: animation.interpolate({
          inputRange: [0.01, 1],
          outputRange: [0, -1 * screenHeight],
          extrapolate: 'clamp',
        }),
      },
    ],
  };

  const loadApp = async () => {
    AsyncStorage.setItem('first', 'true');

    try {
      // let ddd=await AsyncStorage.getItem('currencyid')
      // let curre=await AsyncStorage.getItem('currency')
      let userloggedin = await AsyncStorage.getItem('loggedin');
      setdevicetoken(await AsyncStorage.getItem('devicetoken'));

      // setcurrency(curre)

      // setcurrencyid(ddd);
      if (userloggedin === 'true') {
        let email = await AsyncStorage.getItem('email');
        setemail(email);
        let password = await AsyncStorage.getItem('password');
        setpassword(password);
        setclickable(true);

        setselected('login');

        setnewpassword('');
        setcode('');
        setforgetemail('');
        setcount(0);
        setclickable(true);

        setspinner(true);
        dispatch(LoginUserAction(email.trim(), password, devicetoken));
        //   });
        // });

        // props.navigation.navigate('HomeStack', {
        // });
      } else {
      }

      // alert( );
    } catch (e) {}
  };
  useEffect(() => {
    enn = true;

    loadApp();
    // BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    // sheetRef.current.snapTo(fullheighttt)

    const keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => {
        if (opened) {
          let d = DEVICE_hight / 3 + 200;
          setfullheighttt(d);
          sheetRef.current.snapTo(sheetIsOpen ? fullheighttt : 0);
        }
      }
    );

    const keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        if (opened) {
          setfullheighttt(DEVICE_hight / 3);
          sheetRef.current.snapTo(sheetIsOpen ? fullheighttt : 0);
        }
      }
    );

    return () => {
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
      // BackHandler.removeEventListener('hardwareBackPress', handleBackButtonClick);
    };
  }, []);

  const validateEmail = (email) => {
    var re =
      /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  };

  const openCamera = (cropit, baseprofileimage) => {
    ImagePicker.openCamera({
      width: 500,
      height: 500,
      cropping: cropit,
      cropperCircleOverlay: false,
      sortOrder: 'none',
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperToolbarColor: 'white',
      cropperActiveWidgetColor: 'white',
      cropperToolbarWidgetColor: '#3498DB',
      mediaType: 'photo',
    })
      .then((image) => {
        ImgToBase64.getBase64String(image.path).then((base64String) => {
          if (baseprofileimage) {
            setimagebaseprofile(base64String);
            setimagebaseupload(base64String);
          } else {
            setimagebaseupload(base64String);
          }
        });

        let source = { uri: image.path };
        if (baseprofileimage) {
          setsrcimage(source);
          setsrcuploadimage(source);
        } else {
          setsrcuploadimage(source);
        }
      })
      .catch((e) => {
        hideModalVisible();

        Alert.alert(e.message ? e.message : e);
      });
  };

  const openGallary = (cropit, baseprofileimage) => {
    ImagePicker.openPicker({
      width: 500,
      height: 500,
      cropping: cropit,
      cropperCircleOverlay: false,
      sortOrder: 'none',
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperToolbarColor: 'white',
      cropperActiveWidgetColor: 'white',
      cropperToolbarWidgetColor: '#3498DB',
      mediaType: 'photo',
    })
      .then((image) => {
        ImgToBase64.getBase64String(image.path).then((base64String) => {
          if (baseprofileimage) {
            setimagebaseprofile(base64String);
            setimagebaseupload(base64String);
          } else {
            setimagebaseupload(base64String);
          }
        });

        let source = { uri: image.path };
        if (baseprofileimage) {
          setsrcimage(source);
          setsrcuploadimage(source);
        } else {
          setsrcuploadimage(source);
        }
      })
      .catch((e) => {
        hideModalVisible();

        Alert.alert(e.message ? e.message : e);
      });
  };

  const onLoginPressed = (email, password) => {
    // const {username, password} = this.state;
    if (email.length > 0) {
      if (!validateEmail(email)) {
        Toast.showWithGravity('بريد إلكتروني خاطئ', Toast.LONG, Toast.BOTTOM);
      } else if (password.length < 8) {
        Toast.showWithGravity(
          'طول كلمة المرور غير صالحة',
          Toast.LONG,
          Toast.BOTTOM
        );
      } else {
        setnewpassword('');
        setcode('');
        setforgetemail('');
        setcount(0);
        setclickable(true);
        setselected('login');

        setspinner(true);

        dispatch(LoginUserAction(email.trim(), password, devicetoken));

        // props.navigation.navigate('HomeStack', {
        // });
        // });
      }
    } else {
      Toast.showWithGravity(
        'تحتاج إلى إضافة الحقول المفقودة',
        Toast.LONG,
        Toast.BOTTOM
      );
    }
  };

  const onRegisterPressed = (
    email,
    password,
    name,
    phone,
    address,
    subcityid,
    imagebaseprofile,
    imagebaseupload
  ) => {
    // const {username, password} = this.state;
    if (name !== '' && address !== '' && phone !== '') {
      if (email !== '' && !validateEmail(email)) {
        Toast.showWithGravity('بريد إلكتروني خاطئ', Toast.LONG, Toast.BOTTOM);
      } else if (password !== '' && password.length < 8) {
        Toast.showWithGravity(
          'طول كلمة المرور غير صالحة',
          Toast.LONG,
          Toast.BOTTOM
        );
      } else {
        if (subcityid !== null) {
          // AsyncStorage.setItem('loggedin', "true").then(() => {
          if (email !== '' && password !== '') {
            setclickable(true);

            setspinner(true);
            setselected('register');
            let c = country + ' - ' + cityselected + ' - ' + subcityselected;

            let d = subcityid;
            let f = subcityidd;

            dispatch(
              RegisterAction(
                email.trim(),
                password,
                name,
                phone,
                address,
                c,
                imagebaseprofile,
                imagebaseupload,
                subcityid,
                subcityidd
              )
            );
          } else {
            Toast.showWithGravity(
              'تحتاج إلى إضافة الحقول المفقودة',
              Toast.LONG,
              Toast.BOTTOM
            );
          }
        } else {
          Toast.showWithGravity(
            'تحتاج إلى إضافة الحقول المفقودة',
            Toast.LONG,
            Toast.BOTTOM
          );
        }
        // props.navigation.navigate('HomeStack', {
        // });
        // });
      }
    } else {
      Toast.showWithGravity(
        'تحتاج إلى إضافة الحقول المفقودة',
        Toast.LONG,
        Toast.BOTTOM
      );
    }
  };

  return (
 <View
      style={{
        flexDirection: 'column',
        height: '100%',
        backgroundColor: 'white',
      }}>
        <KeyboardAwareScrollView   style={{
          flexDirection: 'column',
          height: DEVICE_hight}}>
   
      <View
        style={{
          flexDirection: 'column',
          height: DEVICE_hight,
          backgroundColor: 'white',
        }}>
        <ImageBackground
          resizeMode='stretch'
          source={require('../../imgs/loginbg.png')}
          style={{
            height: DEVICE_hight*.8,
            width: '100%',
            alignItems: 'center',
            flexDirection: 'column',
            justifyContent: 'center',
            alignContent: 'center',
          }}>
          <View style={{ height: 30 }}></View>
          <Image
            style={{
              width: 150,
              height: 150,
            }}
            resizeMode='contain'
            source={require('../../imgs/logoooo.png')}></Image>
        <Image
                 source={require('../../imgs/headerlogo.png')}
              style={{tintColor:'white',width:'30%',alignItems:'flex-start',height:20, marginTop: -20}}
              resizeMode='stretch'
                />

          <View
            style={{
              width: '100%',

              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <View
              style={{
                width: '100%',
                marginTop: 30,
                justifyContent: 'flex-start',
                flexDirection: 'row-reverse',
                flexDirection: 'column',
                alignItems: 'flex-start',
              }}>
              <View style={styles.inputWrapper}>
                <TextInput
                  style={styles.input_login}
                  placeholder={'البريد الإلكتروني'}
                  autoCapitalize={'none'}
                  returnKeyType={'done'}
                  autoCorrect={false}
                  onFocus={() => {
                    setnewpassword('');
                    setfullheighttt(0);
                    sheetRef.current.snapTo(sheetIsOpen ? fullheighttt : 0);

                    setcode('');
                    setforgetemail('');
                    setcount(0);
                  }}
                  onChangeText={(email) => {
                    opened = false;
                    setcode('');
                    setforgetemail('');
                    setcount(0);

                    setfullheighttt(0);
                    sheetRef.current.snapTo(sheetIsOpen ? fullheighttt : 0);

                    setemail(email);
                  }}
                  value={email}
                  secureTextEntry={false}
                  placeholderTextColor='#3c9bc6'
                  underlineColorAndroid='transparent'
                />
              </View>
              <View style={styles.inputWrapper}>
                <TextInput
                  secureTextEntry={false}
                  placeholder={'كلمه السر'}
                  returnKeyType={'done'}
                  autoCapitalize={'none'}
                  autoCorrect={false}
                  onFocus={() => {
                    setnewpassword('');
                    setfullheighttt(0);
                    sheetRef.current.snapTo(sheetIsOpen ? fullheighttt : 0);

                    setcode('');
                    setforgetemail('');
                    setcount(0);
                  }}
                  secureTextEntry
                  onChangeText={(password) => {
                    setfullheighttt(0);
                    sheetRef.current.snapTo(sheetIsOpen ? fullheighttt : 0);
                    setnewpassword('');

                    setcode('');
                    setforgetemail('');
                    setcount(0);
                    setpassword(password);
                  }}
                  value={password}
                  style={styles.input_login}
                  placeholderTextColor='#3c9bc6'
                  underlineColorAndroid='transparent'
                />
              </View>
              <View
                style={{
                  width: DEVICE_width,
                  paddingLeft: 30,
                  paddingRight: 30,

                  paddingBottom: 20,

                  flexDirection: 'row',
                  justifyContent: 'flex-end',
                }}>
                <TouchableHighlight
          underlayColor='transparent'
          activeOpacity={0.2}
                  onPress={() => {
                    setfullheighttt(DEVICE_hight / 3);

                    sheetRef.current.snapTo(sheetIsOpen ? fullheighttt : 0);
                    // }
                    opened = true;
                    setnewpassword('');
                    setcode('');
                    setforgetemail('');
                    setcount(0);
                  }}>
                  <Text style={{ color: 'white', marginHorizontal: 10 }}>
                    {'نسيت كلمة المرور'}
                  </Text>
                </TouchableHighlight>
              </View>

              <View
                style={{
                  width: '100%',
                  flexDirection: 'row-reverse',
                  justifyContent: 'center',
                  alignItems: 'center',
                }}>
               <TouchableHighlight
          underlayColor='transparent'
          activeOpacity={0.2}
                  onPress={() => {
                    setcityswipable(false);

                    setsubcityswipable(false);
                    setfullheighttt(0);
                    sheetRef.current.snapTo(sheetIsOpen ? fullheighttt : 0);

                    setforgetemail('');
                    setcount(0);
                    setcode('');
                    setnewpassword('');

                    onLoginPressed(email, password);
                  }}>
                  <LinearGradient
                    colors={['#04b5eb', '#06d4e5', '#06d4e5']}
                    style={{
                      justifyContent: 'center',
                      alignContent: 'center',
                      alignItems: 'center',
                      borderRadius: 40,
                    }}>
                    <Text style={styles.loginText}> {'تسجيل الدخول'}</Text>
                  </LinearGradient>
                </TouchableHighlight>
              </View>

              <View style={[styles.socialContainer]}></View>
            </View>
          </View>
        </ImageBackground>
        <View
          style={{
            width: '100%',
            height:DEVICE_hight*.2,
            justifyContent: 'center',
            flexDirection: 'row-reverse',
          }}>
         <TouchableHighlight
          underlayColor='transparent'
          activeOpacity={0.2}
            onPress={() => {
              handleOpen();
            }}>
            <LinearGradient
              colors={['#04b5eb', '#06d4e5', '#06d4e5']}
              style={{
                justifyContent: 'center',
                alignContent: 'center',
                alignItems: 'center',
                borderRadius: 40,
              }}>
              <Text style={styles.loginText}> {'تسجيل مستخدم جديد'}</Text>
            </LinearGradient>
          </TouchableHighlight>
        </View>
      </View>
</KeyboardAwareScrollView>
      <Animated.View style={[StyleSheet.absoluteFill, styles.cover, backdrop]}>

        <View style={[styles.sheet]}>
          <Animated.View style={[styles.popup, slideUp]}>
            <View style={{ height: '100%' }}>
            <KeyboardAwareScrollView
                showsVerticalScrollIndicator={false}
                style={{ height: '90%', width: '100%' }}>
             <ScrollView>
                <View
                  style={{
                    flexDirection: 'column',
                  }}>
                  <View
                    style={{
                      height: 150,
                      flexDirection: 'column',
                      width: '100%',
                    }}>
                <TouchableHighlight
          underlayColor='transparent'
          activeOpacity={0.2}
                      onPress={() => {
                        // profile=true

                        // if(this.state.clickable===true)
                        // {
                        // if(clickable === true){
                        setimageIsProfile(true);
                        showModalVisible();
                        // selectPhotoTapped(this);

                        setcityswipable(false);
                        setsubcityswipable(false);

                        // }

                        // }else{

                        // }
                      }}>
                      <ImageBackground
                        style={{ width: '100%', height: 300 }}
                        resizeMode='cover'
                        source={srcimage}>
                        <ImageBackground
                          style={{ width: '100%', height: 100 }}
                          source={header}
                          resizeMode='cover'>
                          <View style={{ height: 30 }}></View>
                          <View
                            style={{
                              width: '100%',
                              flexDirection: 'row',
                              padding: 10,
                              justifyContent: 'flex-start',
                              alignItems: 'flex-start',
                            }}>
                            <Icon
                              name='arrow-back'
                              size={25}
                              color='white'
                              onPress={() => handleClose()}
                            />
                          </View>
                        </ImageBackground>
                      </ImageBackground>
                    </TouchableHighlight>
                  </View>

                  <Text
                    style={{
                      width: DEVICE_width - 30,
                      textAlign: 'right',
                      marginRight: 30,
                      marginBottom: 10,
                      color: '#3c9bc6',
                      marginTop: 150,
                    }}>
                    يرجى تحميل صورتك*{' '}
                  </Text>

                  <View style={styles.inputWrapper}>
                    <TextInput
                      style={styles.input}
                      placeholder={'الاسم'}
                      autoCapitalize={'none'}
                      returnKeyType={'done'}
                      autoCorrect={false}
                      onChangeText={(name) => {
                        setcityswipable(false);
                        setfullheighttt(0);

                        setsubcityswipable(false);
                        setnewpassword('');
                        setcode('');
                        setforgetemail('');
                        setcount(0);

                        setname(name);
                      }}
                      value={name}
                      secureTextEntry={false}
                      placeholderTextColor='#3c9bc6'
                      underlineColorAndroid='transparent'
                    />
                  </View>
                  <View style={styles.inputWrapper}>
                    <TextInput
                      style={styles.input}
                      placeholder={'البريد الإلكتروني'}
                      autoCapitalize={'none'}
                      returnKeyType={'done'}
                      autoCorrect={false}
                      onChangeText={(email) => {
                        setfullheighttt(0);

                        setcityswipable(false);

                        setsubcityswipable(false);
                        setnewpassword('');
                        setcode('');
                        setforgetemail('');
                        setcount(0);

                        setemail(email);
                      }}
                      value={email}
                      secureTextEntry={false}
                      placeholderTextColor='#3c9bc6'
                      underlineColorAndroid='transparent'
                    />
                  </View>
                  <View style={styles.inputWrapper}>
                    <TextInput
                      secureTextEntry={true}
                      placeholder={'كلمه السر'}
                      returnKeyType={'done'}
                      autoCapitalize={'none'}
                      autoCorrect={false}
                      secureTextEntry
                      onChangeText={(password) => {
                        setfullheighttt(0);

                        setcityswipable(false);

                        setsubcityswipable(false);
                        setnewpassword('');
                        setcode('');
                        setforgetemail('');
                        setcount(0);

                        setpassword(password);
                      }}
                      value={password}
                      style={styles.input}
                      placeholderTextColor='#3c9bc6'
                      underlineColorAndroid='transparent'
                    />
                  </View>

                  <View style={styles.inputtt}>
                    <Image
                      style={{
                        width: 25,
                        height: 25,
                        marginLeft: 10,
                        borderRadius: 60,
                        overflow: 'hidden',
                      }}
                      resizeMode='cover'
                      source={require('../../imgs/flags/ae.png')}
                    />

                    <TextInput
                      secureTextEntry={false}
                      placeholder={'رقم الهاتف '}
                      returnKeyType={'done'}

                      autoCorrect={false}
                      onChangeText={(phone) => {
                        setphone(phone);
                      }}
                      value={phone}
                      style={{
                        height: 60,
                        width: '80%',
                        textAlign: 'right',
                        color: '#3c9bc6',
                      }}
                      keyboardType={'numeric'}
                      placeholderTextColor='#3c9bc6'
                      underlineColorAndroid='transparent'
                    />
                  </View>
                  <View
                    style={{
                      flexDirection: 'row-reverse',
                      width: DEVICE_width,
                      marginBottom: 0,
                    }}>
                    {country !== 'الدولة' && (
                    <TouchableHighlight
                    underlayColor='transparent'
                    activeOpacity={0.2}
                        onPress={() => {
                          if (citylist.length > 0) {
                            setcityswipable(true);

                            setsubcityswipable(false);
                            setnewpassword('');
                            setcode('');
                            setforgetemail('');
                            setcount(0);
                          } else {
                            let citylist = [];
                            if (
                              typeof GetCountriesReducer.payload !==
                                'undefined' &&
                              typeof GetCountriesReducer.payload.data !==
                                'undefined'
                            ) {
                              for (
                                let f = 0;
                                f < GetCountriesReducer.payload.data.length;
                                f++
                              ) {
                                if (
                                  GetCountriesReducer.payload.data[f]
                                    .country_name_ar === 'الإمارات العربية المتحدة'
                                ) {
                                  if (
                                    GetCountriesReducer.payload.data[f].cities
                                      .length > 0
                                  ) {
                                    for (
                                      let d = 0;
                                      d <
                                      GetCountriesReducer.payload.data[f].cities
                                        .length;
                                      d++
                                    ) {
                                      citylist.push({
                                        id: d + 1,
                                        name: GetCountriesReducer.payload.data[
                                          f
                                        ].cities[d].city_name_ar,
                                        idd: GetCountriesReducer.payload.data[f]
                                          .cities[d].id,
                                      });
                                    }
                                  }
                                }
                              }
                              setcitylist(citylist);
                            } else {
                              citylist = [];
                              setcitylist(citylist);
                            }
                            if (citylist.length > 0) {
                              setcityswipable(true);
                            } else {
                              Toast.showWithGravity(
                                '  لا توجد مدن في هذا البلد',
                                Toast.LONG,
                                Toast.BOTTOM
                              );
                            }
                          }
                        }}>
                        <View
                          style={{
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            backgroundColor: '#f1f2f2',
                            flexDirection: 'row-reverse',
                            width: (DEVICE_width - 70) / 2,
                            borderRadius: 20,
                            borderColor: '#3c9bc6',
                            borderWidth: 1,
                            marginRight: 5,
                            marginLeft: 30,

                            height: 40,
                          }}>
                          <Text
                            style={{
                              fontSize: 13,
                              textAlign: 'center',
                              paddingBottom: 5,
                              paddingTop: 5,
                              marginRight: 20,
                              color: '#3c9bc6',
                            }}>
                            {' '}
                            {cityselected}
                          </Text>

                          <Icond
                            style={{
                              alignSelf: 'center',
                              alignItems: 'center',
                              alignContent: 'center',
                              marginLeft: 10,
                            }}
                            name='chevron-down'
                            size={17}
                            color='#3c9bc6'
                          />
                        </View>
                      </TouchableHighlight>
                    )}
                    {cityselected !== 'المدينة' && (
                      <TouchableHighlight
                      underlayColor='transparent'
                      activeOpacity={0.2}
                        onPress={() => {
                          if (subcitylist.length > 0) {
                            setsubcityswipable(true);
                            setnewpassword('');
                            setcode('');
                            setforgetemail('');
                            setcount(0);

                            setcityswipable(false);
                          } else {
                            Toast.showWithGravity(
                              'لا توجد مناطق في هذه المدينة',
                              Toast.LONG,
                              Toast.BOTTOM
                            );
                          }
                        }}>
                        <View
                          style={{
                            justifyContent: 'space-between',
                            alignItems: 'center',
                            backgroundColor: '#f1f2f2',
                            flexDirection: 'row-reverse',
                            width: (DEVICE_width - 70) / 2,
                            borderRadius: 20,
                            borderColor: '#3c9bc6',
                            borderWidth: 1,

                            height: 40,
                          }}>
                          <Text
                            style={{
                              fontSize: 13,
                              textAlign: 'center',
                              paddingBottom: 5,
                              paddingTop: 5,
                              marginRight: 20,
                              color: '#3c9bc6',
                            }}>
                            {' '}
                            {subcityselected}
                          </Text>

                          <Icond
                            style={{
                              alignSelf: 'center',
                              alignItems: 'center',
                              alignContent: 'center',
                              marginLeft: 10,
                            }}
                            name='chevron-down'
                            size={17}
                            color='#3c9bc6'
                          />
                        </View>
                      </TouchableHighlight>
                    )}
                  </View>
                  <View style={styles.inputWrapperrr}>
                    <TextInput
                      style={styles.inputaddress}
                      placeholder={'العنوان'}
                      multiline={true}
                      autoCapitalize={'none'}
                      returnKeyType={'done'}
                      autoCorrect={false}
                      onChangeText={(address) => {
                        setcityswipable(false);
                        setfullheighttt(0);

                        setsubcityswipable(false);
                        setnewpassword('');
                        setcode('');
                        setforgetemail('');
                        setcount(0);

                        setaddress(address);
                      }}
                      value={address}
                      secureTextEntry={false}
                      placeholderTextColor='#3c9bc6'
                      underlineColorAndroid='transparent'
                    />
                  </View>

                  <View
                    style={{
                      width: DEVICE_width,
                      flexDirection: 'row-reverse',
                      justifyContent: 'space-between',
                      marginBottom: 20,
                      alignItems: 'center',
                    }}>
                    <TouchableHighlight
          underlayColor='transparent'
          activeOpacity={0.2}
                      onPress={(_) => {
                        {
                          setswipeablePanelterms(true);
                        }
                      }}>
                      <Text style={{ marginRight: 30, color: '#3c9bc6' }}>
                        {' '}
                        تم قبول الشروط والأحكام
                      </Text>
                    </TouchableHighlight>
                    <CheckBox
                      onPress={() => {
                        setcityswipable(false);

                        setsubcityswipable(false);
                        setnewpassword('');
                        setcode('');
                        setforgetemail('');
                        setcount(0);

                        setchecked(!checked);
                      }}
                      size={25}
                      containerStyle={{ marginLeft: 20 }}
                      checked={checked}
                    />
                  </View>
                  <View
                    style={{
                      width: '100%',
                      justifyContent: 'center',
                      flexDirection: 'row-reverse',
                    }}>
                  <TouchableHighlight
          underlayColor='transparent'
          activeOpacity={0.2}
                      onPress={() => {
                        if (checked === true) {
                          if (imagebaseprofile != '') {
                            if (imagebaseupload !== '') {
                              setcityswipable(false);

                              setsubcityswipable(false);
                              setnewpassword('');
                              setcode('');
                              setforgetemail('');
                              setcount(0);

                              onRegisterPressed(
                                email,
                                password,
                                name,
                                phone,
                                address,
                                subcityid,
                                imagebaseprofile,
                                imagebaseupload
                              );
                            } else {
                              Toast.showWithGravity(
                                'تحتاج إلى تحميل وثيقة التأكيد',
                                Toast.LONG,
                                Toast.BOTTOM
                              );
                            }
                          } else {
                            Toast.showWithGravity(
                              'تحتاج إلى تحميل صورتك  ',
                              Toast.LONG,
                              Toast.BOTTOM
                            );
                          }
                        } else {
                          Toast.showWithGravity(
                            'تحتاج إلى تأكيد قبول الشروط والأحكام ',
                            Toast.LONG,
                            Toast.BOTTOM
                          );
                        }
                      }}>
                      <LinearGradient
                        colors={['#04b5eb', '#06d4e5', '#06d4e5']}
                        style={{
                          justifyContent: 'center',
                          alignContent: 'center',
                          alignItems: 'center',
                          borderRadius: 40,
                        }}>
                        <Text style={styles.loginText}>{'تسجيل مستخدم'}</Text>
                      </LinearGradient>
                    </TouchableHighlight>
                  </View>
                </View>
                </ScrollView>
                </KeyboardAwareScrollView>

            </View>
          </Animated.View>
        </View>
      </Animated.View>

      <SwipeablePanel
        isActive={cityswipable}
        closeOnTouchOutside={true}
        onClose={this}
        openLarge={true}
        // onClose={this.closePanel}
        // onPressCloseButton={this.closePanel}
      >
        <View
          style={{
            flexDirection: 'column',
            justifyContent: 'space-between',
          }}>
          <View>
            <View style={{ marginTop: 15 }}>
              <SelectableFlatlist
                data={citylist}
                state={STATE.EDIT}
                multiSelect={false}
                checkColor='white'
                uncheckColor='white'
                itemsSelected={(selectedItem) => {
                  (_) => itemsSelectedcity(selectedItem);
                }}
                initialSelectedIndex={[getinitial(cityid)]}
                cellItemComponent={(item, otherProps) => rowcityItem(item)}
              />
            </View>
          </View>
        </View>
      </SwipeablePanel>

      <SwipeablePanel
        isActive={subcityswipable}
        closeOnTouchOutside={true}
        onClose={this}
        openLarge={true}

        // onClose={this.closePanel}
        // onPressCloseButton={this.closePanel}
      >
        <View
          style={{
            flexDirection: 'column',
            justifyContent: 'space-between',
          }}>
          <View>
            <View style={{ marginTop: 15 }}>
              <SelectableFlatlist
                data={subcitylist}
                state={STATE.EDIT}
                multiSelect={false}
                checkColor='white'
                uncheckColor='white'
                itemsSelected={(selectedItem) => {
                  (_) => itemsSelectedsubcity(selectedItem);
                }}
                initialSelectedIndex={getinitial(subcityid)}
                cellItemComponent={(item, otherProps) => rowsubcityItem(item)}
              />
            </View>
          </View>
        </View>
      </SwipeablePanel>

      <SwipeablePanel
        isActive={swipeablePanelterms}
        closeOnTouchOutside={false}
        onClose={this}
        openLarge
        // onClose={this.closePanel}
        // onPressCloseButton={this.closePanel}
      >
        <View
          style={{
            flexDirection: 'column',
            justifyContent: 'space-between',
          }}>
          <View>
            <View style={{ padding: 20 }}>
              <Text
                style={{
                  fontSize: 20,
                  fontWeight: '400',
                  justifyContent: 'center',
                  alignContent: 'center',
                  alignItems: 'center',
                  alignSelf: 'center',
                  color:'#3c9bc6'
                }}>

                الشروط والأحكام
              </Text>
            </View>

            <View
              style={{
                width: DEVICE_width,
                margin: 10,
              }}
            
            >
               <ScrollView
               style={{flexDirection:'column'}}
                contentContainerStyle={{
                  paddingHorizontal: 10,
                  paddingVertical: 5,
                }}>
                {terms.map((item, key) => (
                  <Image  style={{
                    width: DEVICE_width-40,
                    height: DEVICE_hight - 300,
                    marginHorizontal: 10,
                  }}
                  resizeMode='stretch'
                  source={item} />
                ))}
                </ScrollView>
              </View>
          </View>
      

          <TouchableHighlight
          underlayColor='transparent'
          activeOpacity={0.2}
          style={{
            alignItems: 'center',
            marginBottom: 50,
          }}
          onPress={(_) => {
            setchecked(true);
            setswipeablePanelterms(false);
          }}>
          <LinearGradient
            colors={['#04b5eb', '#06d4e5', '#06d4e5']}
            style={{ borderRadius: 20 }}>
            <Text
              style={{
                color: 'white',
                width: '50%',
                marginHorizontal: '25%',
                marginVertical: 8,
                fontSize: 20,
              }}>
                           قبول الشروط

            </Text>
          </LinearGradient>
        </TouchableHighlight>
        </View>
      </SwipeablePanel>
      <BottomSheet
        ref={sheetRef}
        snapPoints={[fullheighttt, 0]}
        borderRadius={20}
        renderHeader={renderHeader}
        enabledInnerScrolling={false}
        renderContent={renderContent}
        onOpenEnd={() => setSheetIsOpen(true)}
        onCloseEnd={() => setSheetIsOpen(false)}
      />

      <Spinner
        visible={spinner}
        textContent={''}
        textStyle={styles.spinnerTextStyle}
      />
      <ActionSheet
        ref={actionSheetRef}
        containerStyle={{
          backgroundColor: 'transparent',
        }}
        elevation={0}>
        <View
          style={{
            backgroundColor: '#E5E5E5',
            margin: 20,
            borderRadius: 10,
          }}>
         <TouchableHighlight
          underlayColor='transparent'
          activeOpacity={0.2}
         
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 50,
            }}
            onPress={() => {
              hideModalVisible();

              setTimeout(() => {
                openCamera(true, imageIsProfile);
              }, 1000);
            }}>
            <Text>الكاميرا</Text>
          </TouchableHighlight>
          <View
            style={{
              borderBottomWidth: StyleSheet.hairlineWidth,
              borderColor: '#707070',
            }}
          />
          <TouchableHighlight
          underlayColor='transparent'
          activeOpacity={0.2}
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 50,
            }}
            onPress={() => {
              hideModalVisible();

              setTimeout(() => {
                openGallary(true, imageIsProfile);
              }, 1000);
            }}>
            <Text>المعرض</Text>
          </TouchableHighlight>
        </View>
        <View
          style={{
            backgroundColor: '#E5E5E5',
            marginHorizontal: 20,
            borderRadius: 10,
            marginBottom: 20,
          }}>
          <TouchableHighlight
          underlayColor='transparent'
          activeOpacity={0.2}
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 50,
            }}
            onPress={() => {
              hideModalVisible();
            }}>
            <Text style={{ color: 'red' }}>إلغاء</Text>
          </TouchableHighlight>
        </View>
      </ActionSheet>
    </View>
 
);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'yellow',
  },
  image: {
    width: 150,
    height: 150,
    borderRadius: 80,
    marginRight: 10,
    marginLeft: 10,
  },
  button: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3888C6',
    height: MARGIN,
    borderRadius: 5,
    zIndex: 100,
  },
  socialContainer: {
    marginLeft: 30,
    marginRight: 30,
    marginTop: 30,
    width: DEVICE_width - 60,
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  circle: {
    height: MARGIN,
    width: MARGIN,
    marginTop: -MARGIN,
    borderWidth: 1,
    borderColor: '#3888C6',
    borderRadius: 100,
    alignSelf: 'center',
    zIndex: 99,
    backgroundColor: '#3888C6',
  },

  image: {
    width: 150,
    height: 150,
    marginBottom: 20,
  },
  text: {
    flex: 1,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    color: 'white',
    backgroundColor: 'transparent',
    marginTop: 5,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  text: {
    color: 'white',
    fontWeight: 'bold',
    backgroundColor: 'transparent',
    marginTop: 30,
    marginBottom: 20,
  },
  btnEye: {
    position: 'absolute',
    top: 10,
    left: 40,
  },
  iconEye: {
    width: 25,
    height: 25,
    tintColor: '#3888C6',
  },
  btnEyeRight: {
    position: 'absolute',
    top: 10,
    right: 40,
  },
  iconEyeRight: {
    width: 25,
    height: 25,
    tintColor: '#3888C6',
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 250,
    borderRadius: 5,
  },
  loginButton: {
    backgroundColor: '#3888C6',
  },
  restoreButtonContainer: {
    width: 250,
    marginBottom: 15,
    alignItems: 'flex-end',
  },
  socialButtonContent: {
    marginRight: 20,
    marginLeft: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  socialIcon: {
    color: '#FFFFFF',
    marginRight: 5,
  },
  socialLoginButton: {
    backgroundColor: '#929292',
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 10,
    borderRadius: 5,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginText: {
    color: '#FFFFFF',
    alignItems: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    margin: 14,
    textAlign: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 30,
    height: 30,
  },
  buttonText: {
    fontSize: 20,
    textAlign: 'center',
    color: 'white',
    paddingHorizontal: 20,
  },
  buttonStyle: {
    borderRadius: 5,
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 30,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  input: {
    backgroundColor: '#f1f2f2',
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 20,
    flexDirection: 'row',
    borderColor: '#3c9bc6',
    color: '#3c9bc6',
  },
  input_login: {
    backgroundColor: 'white',
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 20,
    flexDirection: 'row',
    borderColor: '#3c9bc6',
    color: '#3c9bc6',
  },
  inputaddress: {
    backgroundColor: '#f1f2f2',
    width: DEVICE_width - 60,
    height: 70,
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 40,
    flexDirection: 'row',
    borderColor: '#3c9bc6',
    color: '#3c9bc6',
    borderRadius: 20,
  },

  inputtt: {
    backgroundColor: '#f1f2f2',
    width: DEVICE_width - 60,
    height: 40,
    flexDirection: 'row-reverse',
    marginBottom: 10,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    alignItems: 'center',
    marginHorizontal: 30,
    paddingRight: 20,
    paddingLeft: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 20,
    borderColor: '#3c9bc6',
    color: '#3c9bc6',
  },
  inputDialog: {
    backgroundColor: '#f1f2f2',
    width: DEVICE_width - 100,
    height: 40,
    marginTop: 15,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 20,
    flexDirection: 'row',
    borderColor: '#3c9bc6',
    color: '#3c9bc6',
  },
  inputRight: {
    backgroundColor: '#f1f2f2',
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    textAlign: 'left',
    borderWidth: 1,
    borderRadius: 10,
    flexDirection: 'row',
    borderColor: '#3c9bc6',
    color: '#3c9bc6',
  },
  inputWrapper: {
    flexDirection: 'row-reverse',
    marginBottom: 10,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  inputWrapperr: {
    flexDirection: 'row-reverse',

    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  inputWrapperrr: {
    flexDirection: 'row-reverse',
    marginTop: 10,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  cover: {
    height: '100%',
  },
  sheet: {
    position: 'absolute',
    top: Dimensions.get('window').height,
    left: 0,
    right: 0,
    height: '100%',
    justifyContent: 'flex-end',
  },
  popup: {
    backgroundColor: '#FFF',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: 80,
  },
  header: {
    height: 25,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,

    backgroundColor: 'white',
    alignItems: 'center',
    justifyContent: 'center',
  },
  panelHandle: {
    width: 35,
    height: 6,
    borderRadius: 4,
    backgroundColor: 'rgba(255,255,255,0.7)',
    marginBottom: 0,
  },
});
