import React, { useEffect, useState, useRef } from 'react';
import {
  FlatList,
  View,
  Text,
  Alert,
  TouchableHighlight,
  Platform,
  ImageBackground,
  TextInput,
  Dimensions,
  Image,
  StyleSheet,
  Keyboard,
} from 'react-native';
import { ButtonGroup } from 'react-native-elements';
import LinearGradient from 'react-native-linear-gradient';

import OrdersItems from './OrdersItems';
import DeliveredItems from './DeliveredItems';
import CanceledItems from './CanceledItems';
import EcutedItem from './EcutedItem';
import SpecialItem from './SpecialItem';
import AppMetrics from '../../common/metrics.ts'
import { useDispatch, useSelector } from 'react-redux';
import { GetOrdersAction } from '../../actions/Actions';
import { UpdateOrdersAction } from '../../actions/Actions';
import { AcceptOrderAction } from '../../actions/Actions';
import Spinner from 'react-native-loading-spinner-overlay';
const DEVICE_hight = Dimensions.get('window').height;
import Toast from 'react-native-simple-toast';
import ImagePicker from 'react-native-image-crop-picker';
import ImgToBase64 from 'react-native-image-base64';
import { EmptyAcceptOrder } from '../../actions/Actions';
import PanelWithoutPadding from '../../common/actualComponents/PanelWithoutPadding'
import { LogoutAction } from '../../actions/Actions';
import { BackHandler } from 'react-native';
import ActionSheet from 'react-native-actions-sheet';
let enn = true;
let opened = false;
let ordertype='0';
const component4 = () => <Text>الطلبات</Text>;
const component3 = () => <Text>ضمن التنفيذ </Text>;
const component2 = () => <Text> انتظار التسليم </Text>;
const component1 = () => <Text>تم التسليم </Text>;
const component0 = () => <Text>المرفوض</Text>;
let colorss = '#3888C6';
let selctedf = '';
import Iconk from 'react-native-vector-icons/AntDesign';
import Icon from 'react-native-vector-icons/FontAwesome5';

import BottomSheet from 'reanimated-bottom-sheet';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { ScrollView } from 'react-native-gesture-handler';
const DEVICE_width = Dimensions.get('window').width;

export default function OrdersScreen(props) {
  const dispatch = useDispatch();
  const sheetRefAccept = useRef(null);
  const [SelectRefuse, setSelectRefuse] = useState(false);
  const [SelectAccept, setSelectAccept] = useState(false);
  const [selectedIndexOptions, setselectedIndexOptions] = useState(3);
  const [totalpp, settotalpp] = useState(0);
  const [vendors, setvendors] = useState([]);

  const [spinner, setspinner] = useState(false);
  const [fourthitems, setfourthitems] = useState([]);
  const [thirditems, setthirditems] = useState([]);
  const [seconditems, setseconditems] = useState([]);
  const [firstitems, setfirstitems] = useState([]);
  const [fifthitems, setfifthitems] = useState([]);
  const [DeliverYcOST, setDeliverYcOST] = useState([]);
  const [data, setdata] = useState([]);
  const [currency, setcurrency] = useState('درهم اماراتي');
  const [first, setfirst] = useState(true);
  const [currencyid, setcurrencyid] = useState('1');
  const [selcted, setselcted] = useState('');
  const [dataimages, setdataimages] = useState([]);
  const [pricess, setpricess] = useState([]);
  const [reason, setreason] = useState('');
  const [, updateState] = React.useState();
  const [sheetAcceptIsOpen, setsheetAcceptIsOpen] = useState(false);
  
  const [fullheightttAccept, setfullheightttAccept] = useState(-100);
  const [successimage, setsuccessimage] = useState('');

  const [srcimage, setsrcimage] = useState(require('../../imgs/defaultt.png'));

  const [selectedid, setselectedid] = useState(null);

  const [firstdata, setfirstdata] = useState([]);
  const [fifthdata, setfifthdata] = useState([]);

  const [seconddata, setseconddata] = useState([]);

  const [thirddata, setthirddata] = useState([]);

  const [fourthdata, setfourthdata] = useState([]);

  const [updated, setupdated] = useState(false);

  const LoginReducer = useSelector((state) => state.LoginReducer);
  const GetDeliveryOrdersReducer = useSelector(
    (state) => state.GetDeliveryOrdersReducer
  );
  const UpdateDeliveryOrederReducer = useSelector(
    (state) => state.UpdateDeliveryOrederReducer
  );
  const AcceptOrderReducer = useSelector((state) => state.AcceptOrderReducer);
  const GetCountriesReducer = useSelector((state) => state.GetCountriesReducer);
  const actionSheetRef = useRef();
  const showModalVisible = () => {
    actionSheetRef.current?.setModalVisible(true);
  };
  const hideModalVisible = () => {
    actionSheetRef.current?.setModalVisible(false);
  };

  actionSheetRef.current?.snapToOffset(300);

  closepanell = (index) => {
    setSelectRefuse(false)
    setSelectAccept(false)

    opened = false;
  };
  function handleBackButtonClick() {
    setSelectRefuse(false)
    setSelectAccept(false)

    opened = false;
    // setopened(false)

    return true;
  }
  const renderHeader = () => {
    return (
      <View style={styles.header}>
        <View style={styles.panelHandle} />
      </View>
    );
  };



  // const getcityname=(id,list)=>
  // {
  // for(let r=0;r<list.length;r++)
  // {
  //   if(list[r].idd === id)
  //   {
  //     return list[r].name;
  //   }
  // }
  // }
  const getvendor = (vendors, id) => {
    let i = '';
    for (let f = 0; f < vendors.length; f++) {
      if ('' + vendors[f].order_id === '' + id) {
        i = vendors[f].vendor_info[0];
      }
    }
    return i;
  };

  if (enn) {
    try {
      if (
        typeof GetCountriesReducer !== 'undefined' &&
        typeof GetCountriesReducer.error !== '' &&
        GetCountriesReducer.error == 'success'
      ) {
        let city = [];

        for (let i = 0; i < GetCountriesReducer.payload.data.length; i++) {
          //      let d= LoginReducer
          //
          //       if(GetCountriesReducer.payload.data[i].country_name === country)

          // {
          for (
            let g = 0;
            g < GetCountriesReducer.payload.data[i].subcities.length;
            g++
          ) {
            city.push({
              id: g + 1,
              name: GetCountriesReducer.payload.data[i].subcities[g]
                .subcity_name,
              idd: GetCountriesReducer.payload.data[i].subcities[g].id,
            });
          }
          // }
        }
        // setcityid(parseInt(LoginReducer.payload.data.address[1].lat))

        enn = false;
      }
    } catch (e) {}
  }

  failOrder = (id,type) => {
    selctedf = 'refuse';

    setreason('');
    setsuccessimage('');
    setselectedid(id);
    opened = true;
    ordertype=type;

    // setopened(true)

    setSelectAccept(false)
    setsrcimage(require('../../imgs/defaultt.png'))
    setSelectRefuse(true)
    sheetRefAccept.current.snapTo(sheetAcceptIsOpen ? fullheightttAccept : 0);

    // Alert.alert('Simple Button pressed')
  };
  confirmorder = (id,type) => {
    selctedf = '';
    setsrcimage(require('../../imgs/defaultt.png'))

    setSelectAccept(true)
  ordertype=type;

    setreason('');
    setsuccessimage('');
    setselectedid(id);
    // forceUpdate();
  };
  acceptorder = (id) => {
    setspinner(true);
    dispatch(AcceptOrderAction(LoginReducer.payload.token.token, id));
    setselcted('acceptorder');
    setSelectAccept(false)
   
    selctedf = '';
  };

  acceptSpecialorder = (id) => {
    setspinner(true);
    dispatch(AcceptOrderAction(LoginReducer.payload.token.token, id));

    // dispatch(
    //   UpdateOrdersAction(
    //     id,
    //     LoginReducer.payload.token.token,
    //     1,
    //     '',
    //     successimage
    //   )
    // );
  
    setselcted('acceptorder');
    setSelectRefuse(false)
    setSelectAccept(false)
    selctedf = '';
  };

  const getitems = (items, id, index) => {
    let arrayy = [];
    for (let s = 0; s < items.length; s++) {
      if ('' + items[s].order_id === '' + id[index].id) {
        arrayy.push(items[s]);
      }
    }

    return arrayy;
  };

  const getunrepeated = (s) => {
    let firstdata = [];
    let seconddata = [];
    let thirddata = [];
    let fourthdata = [];
    let fifthdata = [];
    let firstitems = [];
    let seconditems = [];
    let thirditems = [];
    let fourthitems = [];
    let fifthitems = [];
    for (let q = 0; q < s.length; q++) {
      let ff = LoginReducer.payload.data.address[1].address_name.split('-');

      // if((s[q].destination_address).includes(ff[0])){

      if (s[q].status === '' + 3) {
        firstdata.push(s[q]);

        for (let j = 0; j < s[q].orders.length; j++) {
          if (s[q].orders[j].order_id === s[q].id) {
            firstitems.push(s[q].orders[j]);
          } else {
          }
        }
      } else if ((s[q].status === '' + 4&&''+s[q].type==='0') || s[q].status === '' + 2) {
        seconddata.push(s[q]);

        for (let j = 0; j < s[q].orders.length; j++) {
          if (s[q].orders[j].order_id === s[q].id) {
            seconditems.push(s[q].orders[j]);
          } else {
          }
        }
      } else if (s[q].status === '' + 9) {
        thirddata.push(s[q]);
        for (let j = 0; j < s[q].orders.length; j++) {
          if (s[q].orders[j].order_id === s[q].id) {
            thirditems.push(s[q].orders[j]);
          } else {
          }
        }
      } else if ((s[q].status === '' + 10||((s[q].status === '' + 4)&& ''+s[q].type==='1'))) {
        fourthdata.push(s[q]);
        for (let j = 0; j < s[q].orders.length; j++) {
          if (s[q].orders[j].order_id === s[q].id) {
            fourthitems.push(s[q].orders[j]);
          } else {
          }
        }
        debugger;
      } else if (s[q].status === '' + 0) {
       
        fifthdata.push(s[q]);
        for (let j = 0; j < s[q].orders.length; j++) {
        
          if (s[q].orders[j].order_id === s[q].id) {
         
            fifthitems.push(s[q].orders[j]);
          } else {
          }
        }
      }
      // }
    }

    setfirstdata(firstdata);
    setseconddata(seconddata);
    setthirddata(thirddata);
    setfourthdata(fourthdata);
    setfifthdata(fifthdata);

    setfirstitems(firstitems);
    setseconditems(seconditems);
    setthirditems(thirditems);
    setfourthitems(fourthitems);
    setfifthitems(fifthitems);

    settotalpp(GetDeliveryOrdersReducer.payload.total_payment);

    if (Platform.OS === 'ios') {
      setInterval(() => {
        setspinner(false);
      }, 1000);
    } else {
      // setInterval(() => {

      setspinner(false);

      // }, 1000);
    }

    return s;
  };

  const openCamera = (cropit, baseprofileimage) => {
    ImagePicker.openCamera({
      width: 500,
      height: 500,
      cropping: cropit,
      cropperCircleOverlay: false,
      sortOrder: 'none',
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperToolbarColor: 'white',
      cropperActiveWidgetColor: 'white',
      cropperToolbarWidgetColor: '#3498DB',
      mediaType: 'photo',
    })
      .then((image) => {
        ImgToBase64.getBase64String(image.path).then((base64String) => {
          setsuccessimage(base64String);
        });

        let source = { uri: image.path };
        setsrcimage(source);
      })
      .catch((e) => {
        hideModalVisible();

        Alert.alert(e.message ? e.message : e);
      });
  };

  const openGallary = (cropit) => {
    ImagePicker.openPicker({
      width: 500,
      height: 500,
      cropping: cropit,
      cropperCircleOverlay: false,
      sortOrder: 'none',
      compressImageMaxWidth: 1000,
      compressImageMaxHeight: 1000,
      compressImageQuality: 1,
      includeExif: true,
      cropperStatusBarColor: 'white',
      cropperToolbarColor: 'white',
      cropperActiveWidgetColor: 'white',
      cropperToolbarWidgetColor: '#3498DB',
      mediaType: 'photo',
    })
      .then((image) => {
        ImgToBase64.getBase64String(image.path).then((base64String) => {
          setsuccessimage(base64String);
        });

        let source = { uri: image.path };
        setsrcimage(source);
      })
      .catch((e) => {
        hideModalVisible();

        Alert.alert(e.message ? e.message : e);
      });
  };

  if (selcted === 'getorders') {
    if (
      typeof GetDeliveryOrdersReducer !== 'undefined' &&
      GetDeliveryOrdersReducer.error === 'success'
    ) {
      setdata(GetDeliveryOrdersReducer.payload.order);

      getunrepeated(GetDeliveryOrdersReducer.payload.order);

      let s = GetDeliveryOrdersReducer.payload.order;
      setdataimages(GetDeliveryOrdersReducer.payload.item_photo);
      setpricess(GetDeliveryOrdersReducer.payload.convert_price);
      setvendors(GetDeliveryOrdersReducer.payload.vendors);
      setDeliverYcOST(GetDeliveryOrdersReducer.payload.delivary_cost);

      setselcted('');
    } else if (
      typeof GetDeliveryOrdersReducer !== 'undefined' &&
      GetDeliveryOrdersReducer.error !== 'success' &&
      GetDeliveryOrdersReducer.error !== ''
    ) {
      if (Platform.OS === 'ios') {
        setInterval(() => {
          setspinner(false);
        }, 1000);
      } else {
        // setInterval(() => {

        setspinner(false);

        // }, 1000);
      }
      setdata([]);
      setdataimages([]);
      setselcted('');
      setpricess([]);
      setvendors([]);
    }
  } else if (selcted === 'update') {
    if (
      typeof UpdateDeliveryOrederReducer !== 'undefined' &&
      UpdateDeliveryOrederReducer.error !== ''
    ) {
      setupdated(false);
      if (
        typeof UpdateDeliveryOrederReducer !== 'undefined' &&
        UpdateDeliveryOrederReducer.error === 'success'
      ) {
        dispatch(
          GetOrdersAction(
            currencyid,
            LoginReducer.payload.token.token,
            LoginReducer.payload.data.id
          )
        );
setSelectRefuse(false)
setSelectAccept(false)


        setselcted('getorders');
      } else {
        setselcted('');
        if (Platform.OS === 'ios') {
          setInterval(() => {
            setspinner(false);
          }, 1000);
        } else {
          // setInterval(() => {

          setspinner(false);

          // }, 1000);
        }
        Toast.showWithGravity(
          'حدث خطأ ، يرجى المحاولة لاحقًا  ',
          Toast.LONG,
          Toast.BOTTOM
        );
      }
    }
  } else if (selcted === 'acceptorder') {
    if (
      typeof AcceptOrderReducer !== 'undefined' &&
      AcceptOrderReducer.error !== ''
    ) {
      dispatch(
        GetOrdersAction(
          currencyid,
          LoginReducer.payload.token.token,
          LoginReducer.payload.data.id
        )
      );
      setselcted('getorders');
      dispatch(EmptyAcceptOrder());
    }
  }

  const updateIndexOptions = (selectedIndex) => {
    setSelectRefuse(false)
    setSelectAccept(false)

    if (selectedIndex === 0) {
      colorss = 'red';
    } else if (selectedIndex === 1) {
      colorss = 'green';
    } else {
      colorss = '#3888C6';
    }
    setselectedIndexOptions(selectedIndex);
  };

  const rowItem = (item) => (
    <View
      style={{
        flex: 1,
        borderBottomWidth: 0.0,
        alignItems: 'stretch',
        paddingLeft: 30,
        paddingRight: 30,
        justifyContent: 'space-between',
        paddingVertical: 20,
        flexDirection: 'row-reverse',
        width: '100%',
        borderBottomColor: 'white',
      }}>
      <Text style={{ fontSize: 18 }}>{item.test}</Text>
    </View>
  );
  // const loadApp = async () => {

  //   // setcurrencyid(props.navigation.state.params.currencyid);

  //   // setcurrency(props.navigation.state.params.currency);

  //   // setiddd(props.navigation.state.params.id);
  //   }

  useEffect(() => {
    // setisLoading(true);
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);
    const keyboardDidShowListener = Keyboard.addListener(
      'keyboardDidShow',
      () => {
        if (opened) {
          let d = 500;
          if (selctedf === 'refuse') {
            setsrcimage(require('../../imgs/defaultt.png'))

            setSelectAccept(false)
      setSelectRefuse(true)
        
          } else if (selctedf === 'accept') {
            setsrcimage(require('../../imgs/defaultt.png'))

            setSelectRefuse(false)
            setSelectAccept(true)
          }
        }
      }
    );

    const keyboardDidHideListener = Keyboard.addListener(
      'keyboardDidHide',
      () => {
        if (opened) {
          if (selctedf === 'refuse') {
            setsrcimage(require('../../imgs/defaultt.png'))
            setSelectRefuse(true)
            setSelectAccept(false)
         
          } else if (selctedf === 'accept') {
            setsrcimage(require('../../imgs/defaultt.png'))
            setSelectRefuse(false)
            setSelectAccept(true)
          }
        }
      }
    );

    // loadApp();
    if (first) {
      if (
        typeof LoginReducer !== 'undefined' &&
        typeof LoginReducer.payload !== 'undefined' &&
        typeof LoginReducer.payload.data !== 'undefined'
      ) {
        setselcted('getorders');

        dispatch(
          GetOrdersAction(
            currencyid,
            LoginReducer.payload.token.token,
            LoginReducer.payload.data.id
          )
        );
        setfirst(false);
      }
    }
    const timer = window.setInterval(() => {
      if (
        typeof LoginReducer !== 'undefined' &&
        typeof LoginReducer.payload !== 'undefined' &&
        typeof LoginReducer.payload.data !== 'undefined'
      ) {
        setselcted('getorders');

        dispatch(
          GetOrdersAction(
            currencyid,
            LoginReducer.payload.token.token,
            LoginReducer.payload.data.id
          )
        );
      }
    }, 50000);
    return () => {
      // Return callback to run on unmount.
      window.clearInterval(timer);
      keyboardDidHideListener.remove();
      keyboardDidShowListener.remove();
      BackHandler.removeEventListener(
        'hardwareBackPress',
        handleBackButtonClick
      );
    };
  }, [GetDeliveryOrdersReducer]);

  return (
    <View
      style={{
        height: '100%',
        width: '100%',
        flexDirection: 'column',

        justifyContent: 'space-between',
      }}>
      <View
        style={{
          height: '100%',
          width: '100%',
          flexDirection: 'column',

          justifyContent: 'space-between',
        }}>
        <ImageBackground
          style={{
            width: '100%',
            height: 150,
            flexDirection: 'row-reverse',
            justifyContent: 'space-between',
          }}
          resizeMode='stretch'
          source={require('../../imgs/headerr.png')}>
          <View
            style={{
              width: '100%',
              marginTop: 70,
              paddingHorizontal: 30,
              flexDirection: 'row-reverse',
              justifyContent: 'space-between',
            }}>
            <Icon size={25} color='white' />
            {/* <Icon
              name='history'
              size={25}
              color='white'
              onPress={(_) => {
                props.navigation.navigate('PaymentScreen', {});
              }}
           
            /> */}
               <Image
                 source={require('../../imgs/headerlogo.png')}
              style={{tintColor:'white',width:'30%',alignItems:'flex-start',height:20}}
              resizeMode='stretch'
                />
            <Iconk
              name='user'
              size={30}
              color='white'
              onPress={(_) => {
                props.navigation.navigate('Profile', {});
              }}
            />
          </View>
        </ImageBackground>

        <View
          style={{ height: '100%', width: '100%', backgroundColor: 'white' }}>
          <ButtonGroup
            onPress={(index) => {
              updateIndexOptions(index);
            }}
            selectedIndex={selectedIndexOptions}
            innerBorderStyle={{ color: '#f2f2f2' }}
            buttons={[
              { element: component0 },
              { element: component1 },
              { element: component3 },
              { element: component4 },
            ]}
            textStyle={{ color: 'white' }}
            selectedButtonStyle={{
              backgroundColor: colorss,
              borderRadius: 30,
            }}
            underlayColor='#3888C6'
            containerStyle={{
              height: 50,
              borderRadius: 30,
              borderBottomColor: '#f2f2f2',
              borderColor: '#f2f2f2',
              backgroundColor: '#f2f2f2',
              width: '94%',
              marginLeft: '3%',
              marginRight: '3%',
            }}
          />
<View style={{marginBottom:150}}>
          {typeof fourthdata !== 'undefined' &&
            fourthdata.length > 0 &&
            selectedIndexOptions === 0 && (
              <FlatList
                style={{ width:'100%',height:'90%' }}
                showsVerticalScrollIndicator={false}
                data={fourthdata}
                renderItem={({ item, index }) => (
                  <CanceledItems
                    item={getitems(fourthitems, fourthdata, index)}
                    images={dataimages}
                    data={fourthdata[index]}
                    index={index}
                    iddddd={fourthdata[index].id}
                    pricestotal={totalpp}
                    imageh={(0.2 * DEVICE_hight) / 2}
                    currency={currency}
                    prices={pricess}
                    parentFlatList={this}
                  />
                )}
                keyExtractor={(item) => item.id}
              />
            )}
          {typeof thirddata !== 'undefined' &&
            thirddata.length > 0 &&
            selectedIndexOptions === 1 && (
              <FlatList
              style={{ width:'100%',height:'90%' }}
              data={thirddata}
                showsVerticalScrollIndicator={false}
                renderItem={({ item, index }) => (
                  <DeliveredItems
                    item={getitems(thirditems, thirddata, index)}
                    images={dataimages}
                    data={thirddata[index]}
                    iddddd={thirddata[index].id}
                    index={index}
                    prices={pricess}
                    pricestotal={totalpp}
                    imageh={(0.2 * DEVICE_hight) / 2}
                    currency={currency}
                    parentFlatList={this}
                  />
                )}
                keyExtractor={(item) => item.id}
              />
            )}

          {typeof seconddata !== 'undefined' &&
            seconddata.length > 0 &&
            selectedIndexOptions === 2 && (
              <FlatList
                showsVerticalScrollIndicator={false}
                style={{width:'100%',height:'90%' }}
                data={seconddata}
                renderItem={({ item, index }) => (
                  <OrdersItems
                    index={index}
                    item={getitems(seconditems, seconddata, index)}
                    images={dataimages}
                    pricestotal={totalpp}
                    imageh={(0.2 * DEVICE_hight) / 2}
                    data={seconddata[index]}
                    iddddd={seconddata[index].id}
                    currency={currency}
                    prices={pricess}
                    parentFlatList={this}
                  />
                )}
                keyExtractor={(item) => item.id}
              />
            )}
            <ScrollView style={{width:'100%',height:'100%'}}>
            <View>
          {typeof fifthdata !== 'undefined' &&
            fifthdata.length > 0 &&
            selectedIndexOptions === 3 && (
              <Text
                style={{
                  fontSize: 18,
                  marginHorizontal: 15,
                  marginVertical: 5,
                  textAlign: 'right',
                }}>
                الطلبات الخاصة
              </Text>
            )}
          {typeof fifthdata !== 'undefined' &&
            fifthdata.length > 0 &&
            selectedIndexOptions === 3 && (
              <View style={{ flexDirection: 'column' }}>
                <FlatList
                  showsVerticalScrollIndicator={false}
                  data={fifthdata}
                  renderItem={({ item, index }) => (
                    <SpecialItem
                      item={getitems(fifthitems, fifthdata, index)}
                      images={dataimages}
                      delivarycost={DeliverYcOST}
                      data={fifthdata[index]}
                      iddddd={fifthdata[index].id}
                      vendor={getvendor(vendors, fifthdata[index].id)}
                      index={index}
                      pricestotal={totalpp}
                      imageh={(0.2 * DEVICE_hight) / 2}
                      currency={currency}
                      prices={pricess}
                      parentFlatList={this}
                    />
                  )}
                  keyExtractor={(item) => item.id}
                />
              </View>
            )}
          {typeof firstdata !== 'undefined' &&
            firstdata.length > 0 &&
            selectedIndexOptions === 3 && (
              <View style={{ flexDirection: 'column' }}>
                <FlatList
                  showsVerticalScrollIndicator={false}
                  style={{ marginBottom: 100,width:'100%',height:'90%' }}
                  data={firstdata}
                  renderItem={({ item, index }) => (
                    <EcutedItem
                      item={getitems(firstitems, firstdata, index)}
                      images={dataimages}
                      data={firstdata[index]}
                      iddddd={firstdata[index].id}
                      vendor={getvendor(vendors, firstdata[index].id)}
                      index={index}
                      pricestotal={totalpp}
                      imageh={(0.2 * DEVICE_hight) / 2}
                      currency={currency}
                      prices={pricess}
                      parentFlatList={this}
                    />
                  )}
                  keyExtractor={(item) => item.id}
                />
              </View>
            )}
          {firstdata.length === 0&&fifthdata.length === 0  && selectedIndexOptions === 3 && (
            <View
              style={{
                width: '100%',
                height: DEVICE_hight - 100,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  size: 25,
                  fontWeight: 'bold',
                  color: '#3888C6',
                }}>
                لا توجد طلبات  
              </Text>
            </View>
          )} 
          </View>
          </ScrollView>
      
          {seconddata.length === 0 && selectedIndexOptions === 2 && (
            <View
              style={{
                width: '100%',
                height: DEVICE_hight - 100,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  size: 25,
                  fontWeight: 'bold',
                  color: '#3888C6',
                }}>
                لا توجد طلبات سابقة
              </Text>
            </View>
          )}
          {thirddata.length === 0 && selectedIndexOptions === 1 && (
            <View
              style={{
                width: '100%',
                height: DEVICE_hight - 100,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  size: 25,
                  fontWeight: 'bold',
                  color: '#3888C6',
                }}>
                لا توجد طلبات سابقة
              </Text>
            </View>
          )}
          {fourthdata.length === 0 && selectedIndexOptions === 0 && (
            <View
              style={{
                width: '100%',
                height: DEVICE_hight - 100,
                justifyContent: 'center',
                alignItems: 'center',
              }}>
              <Text
                style={{
                  textAlign: 'center',
                  size: 25,
                  fontWeight: 'bold',
                  color: '#3888C6',
                }}>
                لا توجد طلبات سابقة
              </Text>
            </View>
          )}
          </View>
        </View>

        <Spinner
          visible={spinner}
          textContent={''}
          textStyle={{ color: '#FFF' }}
        />
      </View>
      <PanelWithoutPadding
          isActive={SelectRefuse}
          closeOnTouchOutside={true}
          onClose={this}
          closeeee={false}
          openLarge={true}
          style={{
            width: AppMetrics.screenWidth,
            flexDirection: 'column',
            justifyContent:'center',
            alignItems:'center',
            height: AppMetrics.screenHeight * 0.30,
          }}>
            
             <View
      style={{
        flexDirection: 'column',
        height: AppMetrics.screenHeight * 0.30,
        backgroundColor: 'white',
        borderTopLeftRadius:30,borderTopRightRadius:30,
        paddingVertical:20
        
      }}>
      <Text
        style={{
          fontSize: 17,
          fontWeight: '300',
          justifyContent: 'center',
          alignContent: 'center',
          alignItems: 'center',
          alignSelf: 'center',
        }}>
        سبب إلغاء الطلب
      </Text>

      <View style={styles.inputWrapper}>
        <TextInput
          style={styles.inputaddress}
          placeholder={' السبب '}
          multiline={true}
          autoCapitalize={'none'}
          returnKeyType={'next'}
          autoCorrect={false}
          onChangeText={(reason) => {
            opened = true;
            setreason(reason);
          }}
          value={reason}
          secureTextEntry={false}
          placeholderTextColor='grey'
          underlineColorAndroid='transparent'
        />
      </View>
<View style={{flexDirection:'row-reverse'}}>
      <LinearGradient
        colors={['#04b5eb', '#06d4e5', '#06d4e5']}
        style={{
          borderRadius: 30,
          width: '40%',
          marginHorizontal: '5%',
          textAlign: 'center',
          justifyContent: 'center',
          alignItems: 'center',
          height: 40,
        }}>
        <TouchableHighlight
          underlayColor='transparent'
          activeOpacity={0.2}
          style={{
            textAlign: 'center',
            alignItems: 'center',
          }}
      
        onPress={() => {
          if (reason.length > 0) {
            setselcted('update');
            if(ordertype==='0'){
            dispatch(
              UpdateOrdersAction(
                selectedid,
                LoginReducer.payload.token.token,
                10,
                reason,
                successimage
              )
            );
              }else{
                dispatch(
                  UpdateOrdersAction(
                    selectedid,
                    LoginReducer.payload.token.token,
                    4,
                    reason,
                    successimage
                  )
                );
              }
            

            setspinner(true);
            selctedf = '';
            setSelectRefuse(false)
          } else {
            Toast.showWithGravity(
              'يرجى تحديد سبب الإلغاء  ',
              Toast.LONG,
              Toast.BOTTOM
            );
          }
        }}>
        <Text
          style={{
            color: 'white',
            fontSize: 20,
            fontWeight: '400',
            textAlign: 'center',
         
            justifyContent:'center',
            textAlign:'center'
          }}>
          {' '}
          إرسال{' '}
        </Text>
      </TouchableHighlight>
      </LinearGradient>
      <TouchableHighlight
          underlayColor='transparent'
          activeOpacity={0.2}
          style={{
            textAlign: 'center',
            alignItems: 'center',
            textAlign: 'center',
            justifyContent: 'center',
            alignItems: 'center',
            height: 40,
            backgroundColor:'grey',borderRadius:30,marginHorizontal:'5%',
            width:'40%'
          }}
      
        onPress={() => {
      setSelectRefuse(false)
        }}>
        <Text
          style={{
            color: 'white',
            fontSize: 20,
            fontWeight: '400',
            textAlign: 'center',
         
            justifyContent:'center',
            textAlign:'center'
          }}>
          الإلغاء
        </Text>
      </TouchableHighlight>
</View>
    </View>
            </PanelWithoutPadding>
 
            <PanelWithoutPadding
          isActive={SelectAccept}
          closeOnTouchOutside={true}
          onClose={this}
          closeeee={false}
          openLarge={true}
          style={{
            width: AppMetrics.screenWidth,
            flexDirection: 'column',
            justifyContent:'center',
            alignItems:'center',
            height: AppMetrics.screenHeight * 0.55,
          }}>
            

             <View
      style={{
        flexDirection: 'column',
        height: AppMetrics.screenHeight * 0.55,
        backgroundColor: 'white',
        borderTopLeftRadius:30,borderTopRightRadius:30,
        paddingVertical:20
        
      }}>
      <Text
        style={{
          fontSize: 17,
          marginBottom:5,
          fontWeight: '300',
          justifyContent: 'center',
          alignContent: 'center',
          alignItems: 'center',
          alignSelf: 'center',
        }}>
        تأكيد تسليم الطلب عن طريق التقاط صورة هوية العميل
      </Text>

      <View style={styles.inputWrapper}>
      <TouchableHighlight
          underlayColor='transparent'
          activeOpacity={0.2}
        style={{ width: '100%', height: 280 }}
        onPress={() => {
          showModalVisible();
        }}>
        <ImageBackground
          resizeMode='cover'
          source={srcimage}
          style={{ width: '100%', height: AppMetrics.screenHeight*.35 }}></ImageBackground>
      </TouchableHighlight>

      </View>
<View style={{flexDirection:'row-reverse',marginTop:10}}>
      <LinearGradient
        colors={['#04b5eb', '#06d4e5', '#06d4e5']}
        style={{
          borderRadius: 30,
          width: '40%',
          marginHorizontal: '5%',
          textAlign: 'center',
          justifyContent: 'center',
          alignItems: 'center',
          height: 40,
        }}>
        <TouchableHighlight
          underlayColor='transparent'
          activeOpacity={0.2}
          style={{
            textAlign: 'center',
            alignItems: 'center',
          }}
      
        onPress={() => {
      
          if (successimage.length > 0) {
            setselcted('update');

            opened = false;
            // setopened(false)

if(ordertype==='0'){
            dispatch(
              UpdateOrdersAction(
                selectedid,
                LoginReducer.payload.token.token,
                9,
                reason,
                successimage
              )
            );
              }else{
                dispatch(
                  UpdateOrdersAction(
                    selectedid,
                    LoginReducer.payload.token.token,
                    3,
                    reason,
                    successimage
                  )
                );
              }
            selctedf = '';
            setsrcimage(require('../../imgs/defaultt.png'))
            setSelectAccept(false)
       
            setspinner(true);
          } else {
            Toast.showWithGravity(
              'يرجى تأكيد تسليم الطلب عن طريق التقاط صورة هوية العميل',
              Toast.LONG,
              Toast.BOTTOM
            );
          }
        }}>
        <Text
          style={{
            color: 'white',
            fontSize: 20,
            fontWeight: '400',
            textAlign: 'center',
         
            justifyContent:'center',
            textAlign:'center'
          }}>
          {' '}
          إرسال{' '}
        </Text>
      </TouchableHighlight>
      </LinearGradient>
      <TouchableHighlight
          underlayColor='transparent'
          activeOpacity={0.2}
          style={{
            textAlign: 'center',
            alignItems: 'center',
            textAlign: 'center',
            justifyContent: 'center',
            alignItems: 'center',
            height: 40,
            backgroundColor:'grey',borderRadius:30,marginHorizontal:'5%',
            width:'40%'
          }}
      
        onPress={() => {
      setSelectAccept(false)
        }}>
        <Text
          style={{
            color: 'white',
            fontSize: 20,
            fontWeight: '400',
            textAlign: 'center',
         
            justifyContent:'center',
            textAlign:'center'
          }}>
          الإلغاء
        </Text>
      </TouchableHighlight>
</View>
    </View>
            </PanelWithoutPadding>
   
      <ActionSheet
        ref={actionSheetRef}
        containerStyle={{
          backgroundColor: 'transparent',
        }}
        elevation={0}>
        <View
          style={{
            backgroundColor: '#E5E5E5',
            margin: 20,
            borderRadius: 10,
          }}>
          <TouchableHighlight
             underlayColor='transparent'
             activeOpacity={0.2}
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 50,
            }}
            onPress={() => {
              hideModalVisible();

              setTimeout(() => {
                openCamera(true);
              }, 1000);
            }}>
            <Text>الكاميرا</Text>
          </TouchableHighlight>
          <View
            style={{
              borderBottomWidth: StyleSheet.hairlineWidth,
              borderColor: '#707070',
            }}
          />
          <TouchableHighlight
             underlayColor='transparent'
             activeOpacity={0.2}
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 50,
            }}
            onPress={() => {
              hideModalVisible();

              setTimeout(() => {
                openGallary(true);
              }, 1000);
            }}>
            <Text>المعرض</Text>
          </TouchableHighlight>
        </View>
        <View
          style={{
            backgroundColor: '#E5E5E5',
            marginHorizontal: 20,
            borderRadius: 10,
            marginBottom: 20,
          }}>
          <TouchableHighlight
             underlayColor='transparent'
             activeOpacity={0.2}
            style={{
              alignItems: 'center',
              justifyContent: 'center',
              height: 50,
            }}
            onPress={() => {
              hideModalVisible();
            }}>
            <Text style={{ color: 'red' }}>إلغاء</Text>
          </TouchableHighlight>
        </View>
      </ActionSheet>
    </View>
  );
}

const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#FFF',
  },
  inputaddress: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 60,
    height: 80,
    marginTop: 5,
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
    borderColor: 'grey',
    color: 'grey',
  },
  inputWrapper: {
    flexDirection: 'row-reverse',
    marginBottom: 10,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },

  inputWrapperr: {
    flexDirection: 'row-reverse',
    width: DEVICE_width - 140,
    marginBottom: 5,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  inputtt: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 60,
    height: 50,
    marginTop: 10,
    flexDirection: 'row-reverse',
    marginLeft: 30,
    marginRight: 30,
    alignItems: 'center',

    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: 'grey',
    color: 'grey',
  },
  header: {
    height: 25,
    borderTopLeftRadius: 10,
    borderTopRightRadius: 10,

    backgroundColor: '#cccccc',
    alignItems: 'center',
    justifyContent: 'center',
  },
  panelHandle: {
    width: 35,
    height: 6,
    borderRadius: 4,
    backgroundColor: 'rgba(255,255,255,0.7)',
    marginBottom: 0,
  },
});
