import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Dimensions,
  ScrollView,
  Button,
} from 'react-native';

import CartItemListOrderWithRating from './CartItemListOrderWithRating';
import moment from 'moment';

const DEVICE_WIDTH = Dimensions.get('window').width;

export default class SpecialItem extends Component {
  gettotalp = (totals, currency, id) => {
    let t = 0;
    for (let f = 0; f < totals.length; f++) {
      if ('' + totals[f].order_id === '' + id) {
        t = parseFloat(totals[f].total_price).toFixed(2) + ' ' + currency;
      }
    }
    return t;
  };

  getworkhours = (from, to) => {
    //  let duration =moment.utc.duration(from.asMilliseconds())
    // let duration = moment.duration(from.diff(offset))
    let duration = moment.duration(from, 'milliseconds');

    let y = parseInt(moment.utc(duration.asMilliseconds()).format('HH'));
    y = y + 2;
    if (y === 25) {
      y = 12 + ' صباحا ';
    } else {
      if (y > 12) {
        y = y - 12;
        y = y + ' مساء ';
      } else {
        y = y + ' صباحا ';
      }
    }

    let durationv = moment.duration(to, 'milliseconds');
    let r = parseInt(moment.utc(durationv.asMilliseconds()).format('HH'));
    r = r + 2;
    if (r === 25) {
      r = 12 + ' صباحا ';
    } else {
      if (r > 12) {
        r = r - 12;
        r = r + ' مساء ';
      } else {
        r = r + ' صباحا ';
      }
    }
    return y + 'إلى ' + r;
  };
  getaddress = (address) => {
    let d;
    let ff = address.split('-');
    d = ff[1] + '-' + ff[2];
    return d;
  };

  countof = (array, currency, pricesarray) => {
    let c = 0;
    try {
      for (let q = 0; q < array.length; q++) {
        for (let d = 0; d < pricesarray.length; d++) {
          if ('' + pricesarray[d].item_id === '' + array[q].item.id) {
            c =
              c +
              parseInt(array[q].item_count) * parseFloat(pricesarray[d].price);
          }
        }
      }
    } catch (e) {}

    return c.toFixed(2) + currency;
  };

  getdatee = (date) => {
    let s = '';
    s = parseFloat(
      date.getDate() +
        '-' +
        parseInt(date.getMonth() + 1) +
        '-' +
        date.getFullYear()
    );

    return s;
  };

  getDeliveryCost = (deliverycosts, id) => {
    let delivery = 0;

    for (let f = 0; f < deliverycosts.length; f++) {
      if ('' + deliverycosts[f].order_id === '' + id) {
        return deliverycosts[f].total_price_delivary.toFixed(2);
      }
    }

    return delivery;
  };

  render() {
    return (
      <View
        style={{
          marginTop: 10,
          borderWidth: 0.5,
          backgroundColor: '#229ac4',
          borderRadius: 10,
          borderColor: '#dddddd',
          flexDirection: 'column',
          width: DEVICE_WIDTH - 20,
          marginLeft: 10,
          marginRight: 10,
          marbackgroundColor: 'white',
        }}>
        <View style={{ flexDirection: 'column' }}>
          <View
            style={{
              flex: 1,
              flexDirection: 'row-reverse',
              justifyContent: 'space-between',
              margin: 10,
            }}>
            <View
              style={{
                flexDirection: 'column',
                justifyContent: 'center',
                fontSize: 10,
              }}>
              <Text
                style={{
                  marginRight: 10,
                  textAlign: 'center',
                  color: 'white',
                  fontSize: 10,
                }}>
                التكلفة الإجمالية
              </Text>
              <Text style={{ marginRight: 10, color: 'white', fontSize: 10 }}>
                {this.gettotalp(
                  this.props.pricestotal,
                  this.props.currency,
                  this.props.iddddd
                )}
              </Text>
            </View>
            {'' + this.props.data.orders[0].expected_delivary_date === '0' && (
              <View style={{ flexDirection: 'column' }}>
                <Text
                  style={{ textAlign: 'center', color: 'white', fontSize: 10 }}>
                  تاريخ التسليم المتوقع{' '}
                </Text>
                <Text
                  style={{ textAlign: 'center', color: 'white', fontSize: 10 }}>
                  اليوم
                </Text>
              </View>
            )}
            {'' + this.props.data.orders[0].expected_delivary_date !== '0' && (
              <View style={{ flexDirection: 'column' }}>
                <Text
                  style={{ textAlign: 'center', color: 'white', fontSize: 10 }}>
                  تاريخ التسليم المتوقع{' '}
                </Text>
                <Text
                  style={{ textAlign: 'center', color: 'white', fontSize: 10 }}>
                  {' '}
                  {this.props.data.orders[0].expected_delivary_date}
                </Text>
              </View>
            )}
            <View style={{ flexDirection: 'column' }}>
              <Text
                style={{ textAlign: 'center', color: 'white', fontSize: 10 }}>
                تكلفة التوصيل
              </Text>
              <Text
                style={{ textAlign: 'center', color: 'white', fontSize: 10 }}>
                {this.getDeliveryCost(
                  this.props.delivarycost,
                  this.props.iddddd
                )}
              </Text>
            </View>
          </View>

          <View style={{ flexDirection: 'row-reverse' }}></View>

          <ScrollView
            showsVerticalScrollIndicator={false}
            contentContainerStyle={{
              paddingHorizontal: 10,
              paddingVertical: 5,
            }}>
            {this.props.item.map((item, key) => (
              <CartItemListOrderWithRating
                itemmm={item}
                images={this.props.images}
                imageh={this.props.imageh}
                parentFlatList={this.props.parentFlatList}
              />
            ))}
          </ScrollView>
          <View style={styles.seprator}></View>

          <View
            style={{
              flexDirection: 'row-reverse',
              paddingLeft: 10,
              paddingRight: 10,
            }}>
            <Text
              style={{
                fontWeight: '200',
                fontSize: 10,
                marginRight: 5,
                color: 'white',
                textAlign: 'right',
              }}>
              العنوان
            </Text>
            <Text
              style={{
                fontWeight: '200',
                fontSize: 10,
                marginRight: 5,
                color: 'white',
              }}>
              {' '}
              :
            </Text>
            <Text
              style={{
                fontWeight: '200',
                fontSize: 10,
                marginLeft: 5,
                color: 'white',
                width: DEVICE_WIDTH - 100,
                textAlign: 'right',
              }}
              numberOfLines={2}>
              {this.props.data.destination_address}
            </Text>
          </View>

          <View
            style={{
              flexDirection: 'row-reverse',
              paddingLeft: 10,
              paddingRight: 10,
            }}>
            <Text
              style={{
                fontWeight: '200',
                fontSize: 10,
                marginRight: 5,
                textAlign: 'right',
                color: 'white',
              }}>
              الهاتف
            </Text>
            <Text
              style={{
                fontWeight: '200',
                fontSize: 10,
                marginRight: 5,
                color: 'white',
              }}>
              {' '}
              :
            </Text>
            <Text
              style={{
                fontWeight: '200',
                fontSize: 10,
                marginLeft: 5,
                width: DEVICE_WIDTH - 100,
                textAlign: 'right',
                color: 'white',
              }}
              numberOfLines={2}>
              {this.props.data.destination_phone}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row-reverse',
              paddingLeft: 10,
              paddingRight: 10,
            }}>
            <Text
              style={{
                fontWeight: '200',
                fontSize: 13,
                marginRight: 5,
                color: 'white',
                textAlign: 'right',
              }}>
              معلومات البائع
            </Text>
          </View>

          <View
            style={{
              flexDirection: 'row-reverse',
              paddingLeft: 10,
              paddingRight: 10,
            }}>
            <Text
              style={{
                fontWeight: '200',
                fontSize: 10,
                marginRight: 5,
                color: 'white',
                textAlign: 'right',
              }}>
              العنوان
            </Text>
            <Text
              style={{
                fontWeight: '200',
                fontSize: 10,
                marginRight: 5,
                color: 'white',
                textAlign: 'right',
              }}>
              :
            </Text>
            <Text
              style={{
                fontWeight: '200',
                fontSize: 10,
                marginRight: 5,
                color: 'white',
                textAlign: 'right',
              }}>
              {this.getaddress(this.props.vendor.address[1].address_name)}-
              {this.props.vendor.address[0].address_name}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row-reverse',
              paddingLeft: 10,
              paddingRight: 10,
            }}>
            <Text
              style={{
                fontWeight: '200',
                fontSize: 10,
                marginRight: 5,
                color: 'white',
                textAlign: 'right',
              }}>
              ساعات العمل
            </Text>
            <Text
              style={{
                fontWeight: '200',
                fontSize: 10,
                marginRight: 5,
                color: 'white',
                textAlign: 'right',
              }}>
              :
            </Text>
            <Text
              style={{
                fontWeight: '200',
                fontSize: 10,
                marginRight: 5,
                color: 'white',
                textAlign: 'right',
              }}>
              من
            </Text>
            <Text
              style={{
                fontWeight: '200',
                fontSize: 10,
                marginRight: 5,
                color: 'white',
                textAlign: 'right',
              }}>
              {this.getworkhours(
                this.props.vendor.work_hours[0].start,
                this.props.vendor.work_hours[0].end
              )}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row-reverse',
              paddingLeft: 10,
              paddingRight: 10,
            }}>
            <Text
              style={{
                fontWeight: '200',
                fontSize: 10,
                marginRight: 5,
                color: 'white',
                textAlign: 'right',
              }}>
              رقم الهاتف : {this.props.vendor.phone[0].phone}
            </Text>
          </View>
          <View
            style={{
              flexDirection: 'row-reverse',
              paddingLeft: 10,
              paddingRight: 10,
              justifyContent: 'space-between',
              marginBottom: 5,
              marginTop: 10,
            }}>
            <TouchableOpacity
              activeOpacity={0.9}
              style={{
                backgroundColor: '#1789bc',
                borderRadius: 5,
                width: DEVICE_WIDTH - 40,
                justifyContent: 'center',
                alignItems: 'center',
                height: 40,
              }}
              onPress={() => {
                try {
                  this.props.parentFlatList.acceptSpecialorder(
                    this.props.data.id
                  );
                } catch (ff) {}
              }}>
              <Text style={{ color: 'white', textAlign: 'center' }}>
                قبول الطلب الخاص
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }
}
const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingTop: Platform.OS === 'ios' ? 20 : 0,
    backgroundColor: '#F5FCFF',
  },

  iconStyle: {
    width: 30,
    height: 30,
    justifyContent: 'flex-end',
    alignItems: 'center',
    tintColor: '#fff',
  },

  sub_Category_Text: {
    fontSize: 18,
    color: '#000',
    padding: 10,
  },

  category_Text: {
    textAlign: 'left',
    color: '#fff',
    fontSize: 21,
    padding: 10,
  },

  category_View: {
    marginVertical: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: '#0091EA',
  },

  Btn: {
    padding: 10,
    backgroundColor: '#FF6F00',
  },
  separator: {
    height: 1,
    backgroundColor: '#CCCCCC',
  },
  page: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  stepLabel: {
    fontSize: 12,
    textAlign: 'center',
    fontWeight: '500',
    color: '#999999',
  },
  stepLabelSelected: {
    fontSize: 12,
    textAlign: 'center',
    fontWeight: '500',
    color: '#4aae4f',
  },
  seprator: {
    height: 0.5,
    marginTop: 4,
    marginLeft: 10,
    marginRight: 10,
    backgroundColor: '#CCCCCC',
  },
});
