import React, { useEffect, useState, useRef } from 'react';
import { ImageBackground, View, Alert, Text } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import Icon from 'react-native-vector-icons/Ionicons';

export default function PaymentScreen(props) {
  const dispatch = useDispatch();

  const [selected, setselected] = useState('');
  const [progress, setprogress] = useState(0.1);

  let bg = require('../../imgs/bg.png');

  const LoginReducer = useSelector((state) => state.LoginReducer);

  useEffect(() => {
    // loadApp();
  }, []);
  useEffect(() => {
    timeoutHandle = setTimeout(() => {
      let newp = parseFloat(progress + progress);
      setprogress(newp);
    }, 100);
  }, [progress]);

  return (
    <View
      style={{
        height: '100%',
        width: '100%',
        flexDirection: 'column',
        backgroundColor: 'white',
        justifyContent: 'space-between',
      }}>
      <View
        style={{
          height: '100%',
          width: '100%',
          flexDirection: 'column',

          justifyContent: 'space-between',
        }}>
        <ImageBackground
          style={{
            width: '100%',
            height: 150,
            flexDirection: 'row-reverse',
            justifyContent: 'space-between',
          }}
          resizeMode='stretch'
          source={require('../../imgs/headerr.png')}>
          <View
            style={{
              width: '100%',
              marginTop: 70,
              paddingHorizontal: 30,
              flexDirection: 'row-reverse',
              justifyContent: 'space-between',
            }}>
            <Icon size={25} color='white' />
            <Text style={{ fontSize: 20, color: 'white' }}>Waddelly</Text>
            <Icon
              name='arrow-back'
              size={30}
              color='white'
              onPress={() => {
                props.navigation.goBack(null);
              }}
            />
          </View>
        </ImageBackground>
      </View>
    </View>
  );
}
