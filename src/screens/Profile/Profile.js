import {
  TextInput,
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  Dimensions,
  SafeAreaView,
  TouchableWithoutFeedback,
  ImageBackground,
  ScrollView,
} from 'react-native';

import ImgToBase64 from 'react-native-image-base64';
import Icon from 'react-native-vector-icons/Ionicons';
import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import AsyncStorage from '@react-native-async-storage/async-storage';
import Toast from 'react-native-simple-toast';
import { BackHandler } from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import { LogoutAction } from '../../actions/Actions';

const DEVICE_width = Dimensions.get('window').width;
const DEVICE_hight = Dimensions.get('window').height;
import { EditDeliveryAction } from '../../actions/Actions';
import { LoginUserAction } from '../../actions/Actions';
let bgg = require('../../imgs/profilebg.png');

export default function Profile(props) {
  const dispatch = useDispatch();
  const LoginReducer = useSelector((state) => state.LoginReducer);

  const [name, setname] = useState('');
  const [saveproduct, setsaveproduct] = useState('حفظ التعديل');
  const [editproduct, seteditproduct] = useState(' تعديل الملف الشخصي');
  const [email, setemail] = useState('');
  const [phone, setphone] = useState('');
  const [devicetoken, setdevicetoken] = useState('fffffffff');
  const [password, setpassword] = useState('');
  const [srcimage, setsrcimage] = useState(require('../../imgs/identity.jpg'));

  const [address, setaddress] = useState('');
  const [token, settoken] = useState('');
  const [clickable, setclickable] = useState(false);
  const [clickableee, setclickableee] = useState(false);
  const [city, setcity] = useState('اختر المدينة ');
  const [textcolor, settextcolor] = useState('grey');
  const [imagebaseprofile, setimagebaseprofile] = useState('');
  const [cityid, setcityid] = useState(1);
  const EditDeliveryProfileReducer = useSelector(
    (state) => state.EditDeliveryProfileReducer
  );
  if (
    typeof EditDeliveryProfileReducer.payload !== 'undefined' &&
    EditDeliveryProfileReducer.payload !== null
  ) {
    if (clickableee === false) {
      if (EditDeliveryProfileReducer.error === 'success') {
        dispatch(LoginUserAction(email, password, devicetoken));
        setclickableee(true);
        Toast.showWithGravity('تم تحديث معلوماتك', Toast.LONG, Toast.BOTTOM);
        props.navigation.goBack(null);
      }
    }
  }
  if (clickableee) {
    if (
      typeof LoginReducer.payload !== 'undefined' &&
      typeof LoginReducer.payload.status !== 'undefined'
    ) {
      if (LoginReducer.payload.status === '2') {
        Toast.showWithGravity(
          'حسابك غير نشط يرجى الاتصال بالإدارة  ',
          Toast.LONG,
          Toast.BOTTOM
        );
        AsyncStorage.setItem('loggedin', 'false').then(() => {
          AsyncStorage.setItem('email', '').then(() => {
            AsyncStorage.setItem('password', '').then(() => {
              AsyncStorage.setItem('token', '').then(() => {
                props.navigation.navigate('LoginStack', {});
              });
            });
          });
        });
      } else if (
        typeof LoginReducer.error !== 'undefined' &&
        LoginReducer.error === 'success'
      ) {
        if (clickableee) {
          AsyncStorage.setItem('loggedin', 'true').then(() => {
            AsyncStorage.setItem('email', email).then(() => {
              AsyncStorage.setItem('password', password).then(() => {
                AsyncStorage.setItem(
                  'token',
                  LoginReducer.payload.token.token
                ).then(() => {
                  let tokenn = LoginReducer.payload.token.token;
                  settoken(tokenn);

                  AsyncStorage.setItem(
                    'id',
                    '' + LoginReducer.payload.data.id
                  ).then(() => {
                    setclickableee(false);
                    // props.navigation.navigate('HomeStack', {
                    //   token:LoginReducer.payload.token.token
                    // });
                  });
                });
              });
            });
          });
        }
      }
    }
  }

  function handleBackButtonClick() {
    // setfullheighttt(100)
    // setKeyboardVisible(false);
    // setfff(false)

    return true;
  }

  const itemsSelected = (selectedItem) => {
    console.log(selectedItem);
  };

  const rowItem = (item) => (
    <TouchableOpacity
      style={{
        flex: 1,
        borderBottomWidth: 0.0,
        alignItems: 'stretch',
        paddingLeft: 30,
        paddingRight: 30,
        justifyContent: 'space-between',
        paddingVertical: 20,
        flexDirection: 'row-reverse',
        width: '100%',
        borderBottomColor: 'white',
      }}
      onPress={(_) => {
        setcity(item.name);
        setcityid(item.id);
        settextcolor('black');
      }}>
      <View>
        <Text style={{ fontSize: 18, color: 'black' }}>{item.name}</Text>
      </View>
    </TouchableOpacity>
  );

  // handleChoosePhoto = () => {
  //   const options = {
  //     noData: true,
  //   };
  //   ImagePicker.launchImageLibrary(options, response => {
  //     if (response.uri) {
  //       setphoto(response);
  //       setavatarSource(response)
  //     }
  //   });
  //   ImagePicker.launchCamera(options, (response) => {
  //     if (response.uri) {
  //       setphoto(response);
  //       setavatarSource(response)
  //     }
  //       });
  // };

  //  const selectPhotoTapped=()=> {
  //     const options = {
  //       quality: 1.0,
  //       maxWidth: 500,
  //       maxHeight: 500,
  //       storageOptions: {
  //         skipBackup: true,
  //         path: "itemfiles"
  //       },
  //     };

  //     ImagePicker.showImagePicker(options, (response) => {

  //       if (response.didCancel) {
  //       } else if (response.error) {
  //       } else if (response.customButton) {
  //       } else {
  //         let source = { uri: response.uri };

  //         ImgToBase64.getBase64String(response.uri)
  //         .then(base64String => {
  //           setimagebaseprofile(base64String);
  //         }
  //           )
  //         .catch(err => {

  //         });

  //         // You can also display the image using data:
  //         // let source = { uri: 'data:image/jpeg;base64,' + response.data };

  //         setsrcimage( source)

  //       }
  //     });
  //   }

  const loadApp = async () => {
    try {
      let password = await AsyncStorage.getItem('password');
      setpassword(password);
      let d = await AsyncStorage.getItem('token');
      settoken(d);
    } catch (e) {}
  };

  useEffect(() => {
    BackHandler.addEventListener('hardwareBackPress', handleBackButtonClick);

    loadApp();
    if (
      typeof LoginReducer.payload !== 'undefined' &&
      typeof LoginReducer.payload.status !== 'undefined'
    ) {
      if (
        typeof LoginReducer.error !== 'undefined' &&
        LoginReducer.error === 'success'
      ) {
        setname(LoginReducer.payload.data.name);
        setaddress(LoginReducer.payload.data.address[0].address_name);
        setcity(LoginReducer.payload.data.address[1].address_name);
        setcityid(parseInt(LoginReducer.payload.data.address[1].lat));
        setemail(LoginReducer.payload.data.email);
        setphone(LoginReducer.payload.data.phone[0].phone);

        setsrcimage({
          uri:
            'http://ecommerce.waddeely.com' + LoginReducer.payload.data.photo,
        });
      }
    }
    return () => {
      BackHandler.removeEventListener(
        'hardwareBackPress',
        handleBackButtonClick
      );
    };
  }, []);

  return (
    <ImageBackground
      style={{
        height: '100%',
        width: '100%',
        flexDirection: 'column',
      }}
      resizeMode='cover'
      source={bgg}>
      <ImageBackground
        style={{ width: '100%', height: 450 }}
        resizeMode='cover'
        source={srcimage}>
        <ImageBackground
          style={{
            width: '100%',
            height: 150,
            flexDirection: 'row-reverse',
            justifyContent: 'space-between',
          }}
          resizeMode='stretch'
          source={require('../../imgs/transheader.png')}>
          <View
            style={{
              width: '100%',
              marginTop: 70,
              paddingHorizontal: 30,
              flexDirection: 'row-reverse',
              justifyContent: 'space-between',
            }}>
            <Icon size={25} color='white' />
            <Image
                 source={require('../../imgs/headerlogo.png')}
              style={{tintColor:'white',width:'30%',alignItems:'flex-start',height:20}}
              resizeMode='stretch'
                />
            <Icon
              name='arrow-back'
              size={30}
              color='white'
              onPress={() => {
                props.navigation.goBack(null);
              }}
            />
          </View>
        </ImageBackground>
      </ImageBackground>

      <ScrollView
        showsVerticalScrollIndicator={false}
        style={{ marginTop: 10, height: DEVICE_hight }}>
        <View
          style={{
            backgroundColor: '',
            width: '100%',
            justifyContent: 'flex-start',
            flexDirection: 'row-reverse',
            height: DEVICE_hight - 260,
            flexDirection: 'column',
            alignItems: 'flex-start',
          }}>
          {/* {clickable === true&&

            <Text style={{width:DEVICE_width-30,textAlign:'right',marginRight:30,marginBottom:10}}>يرجى تحميل  صورتك* </Text>
      } */}
          <View
            style={{
              width: '100%',
              justifyContent: 'center',
              alignItems: 'center',
            }}>
            <Text
              style={{
                textAlign: 'center',
                color: 'white',
                fontSize: 20,
                margin: 20,
                fontWeight: 'bold',
              }}>
              {' '}
              المعلومات الشخصية
            </Text>
          </View>
          {clickable === true && (
            <View style={styles.inputWrapper}>
              <TextInput
                style={styles.input}
                placeholder={'الاسم'}
                autoCapitalize={'none'}
                returnKeyType={'next'}
                autoCorrect={false}
                onChangeText={(name) => setname(name)}
                value={name}
                secureTextEntry={false}
                placeholderTextColor='grey'
                underlineColorAndroid='transparent'
              />
            </View>
          )}

          {clickable === false && (
            <View
              style={{
                flexDirection: 'row-reverse',
                width: DEVICE_width - 60,
                justifyContent: 'space-between',
                marginLeft: 30,
                marginRight: 30,
                paddingTop: 5,
                marginBottom: 8,
              }}>
              <Text
                style={{
                  fontSize: 16,
                  textAlign: 'right',

                  color: 'white',
                }}>
                الاسم:
              </Text>
              <Text
                style={{
                  fontSize: 16,
                  textAlign: 'right',

                  flexDirection: 'row',
                  color: 'white',
                }}>
                {name}
              </Text>
            </View>
          )}
          <View
            style={{
              height: 0.5,
              width: DEVICE_width - 60,
              marginLeft: 30,
              marginRight: 30,
              backgroundColor: '#095c92',
            }}
          />

          <View
            style={{
              flexDirection: 'row-reverse',
              width: DEVICE_width - 60,
              marginLeft: 30,
              marginRight: 30,
              justifyContent: 'space-between',
              paddingTop: 5,
              marginTop: 10,

              marginBottom: 8,
            }}>
            <Text
              style={{
                fontSize: 16,
                textAlign: 'right',

                flexDirection: 'row',
                color: 'white',
              }}>
              {' '}
              البريد الإلكتروني :
            </Text>
            <Text
              style={{
                fontSize: 16,
                textAlign: 'right',

                flexDirection: 'row',
                color: 'white',
              }}>
              {email}
            </Text>
          </View>
          <View
            style={{
              height: 0.5,
              width: DEVICE_width - 60,
              marginLeft: 30,
              marginRight: 30,
              backgroundColor: '#095c92',
            }}
          />
          {clickable === true && (
            <View style={{ marginTop: 10 }}>
              <View style={styles.inputtt}>
                <Image
                  style={{ width: 30, height: 30, marginLeft: 10 }}
                  resizeMode='cover'
                  source={require('../../imgs/flags/ae.png')}
                />

                <TextInput
                  secureTextEntry={false}
                  placeholder={'رقم الهاتف '}
                  autoCorrect={false}
                  onChangeText={(phone) => setphone(phone)}
                  value={phone}
                  keyboardType={'numeric'}
                  placeholderTextColor='grey'
                  underlineColorAndroid='transparent'
                />
              </View>
            </View>
          )}
          {clickable === false && (
            <View
              style={{
                flexDirection: 'row-reverse',
                width: DEVICE_width - 60,
                justifyContent: 'space-between',
                marginLeft: 30,
                marginRight: 30,
                marginTop: 10,
                marginBottom: 8,
              }}>
              <Text
                style={{
                  fontSize: 16,
                  paddingTop: 5,

                  textAlign: 'right',

                  flexDirection: 'row',
                  color: 'white',
                }}>
                {'رقم الهاتف '}
              </Text>

              <Text
                style={{
                  fontSize: 16,
                  paddingTop: 5,

                  textAlign: 'right',

                  flexDirection: 'row',
                  color: 'white',
                }}>
                {phone}
              </Text>
            </View>
          )}
          <View
            style={{
              height: 0.5,
              marginLeft: 30,
              marginRight: 30,
              width: DEVICE_width - 60,
              backgroundColor: '#095c92',
            }}
          />
          {clickable === false && (
            <View
              style={{
                flexDirection: 'row-reverse',
                width: DEVICE_width - 60,
                marginHorizontal: 40,

                justifyContent: 'space-between',
                marginTop: 10,
                marginBottom: 8,
              }}>
              <Text
                style={{
                  fontSize: 16,
                  textAlign: 'right',

                  color: 'white',
                }}>
                العنوان:
              </Text>
              <Text
                style={{
                  fontSize: 16,
                  textAlign: 'right',

                  flexDirection: 'row',
                  color: 'white',
                }}>
                {address}
                {'-'} {city}
              </Text>
            </View>
          )}
          <View
            style={{
              flexDirection: 'row-reverse',
              width: DEVICE_width - 60,
              justifyContent: 'space-between',
              marginLeft: 30,
              marginRight: 30,
            }}>
            <View
              style={{
                backgroundColor: '#ffffff',

                height: 40,
                paddingTop: 5,

                flexDirection: 'row-reverse',

                borderColor: 'grey',
                color: 'grey',
                alignItems: 'center',
              }}></View>
          </View>
          <View style={{ width: '100%', justifyContent: 'center' }}>
            <TouchableOpacity
              style={{
                alignItems: 'center',
                marginTop: 20,
              }}
              onPress={() => {
                AsyncStorage.setItem('loggedin', 'false').then(() => {
                  AsyncStorage.setItem('email', 'null').then(() => {
                    AsyncStorage.setItem('password', 'null').then(() => {
                      AsyncStorage.setItem('token', 'null').then(() => {
                        dispatch(
                          LogoutAction(LoginReducer.payload.token.token)
                        );

                        props.navigation.navigate('LoginStack', {});
                      });
                    });
                  });
                });
              }}>
              <LinearGradient
                colors={['#04b5eb', '#06d4e5', '#06d4e5']}
                style={{ borderRadius: 20 }}>
                <Text
                  style={{
                    color: 'white',
                    marginHorizontal: 25,
                    marginVertical: 8,
                    fontSize: 20,
                  }}>
                  تسجيل خروج
                </Text>
              </LinearGradient>
            </TouchableOpacity>
          </View>
          {clickable === true && (
            <View style={styles.inputWrapper}>
              <TextInput
                style={styles.inputaddress}
                placeholder={'العنوان'}
                multiline={true}
                autoCapitalize={'none'}
                returnKeyType={'next'}
                autoCorrect={false}
                onChangeText={(address) => setaddress(address)}
                value={address}
                secureTextEntry={false}
                placeholderTextColor='grey'
                underlineColorAndroid='transparent'
              />
            </View>
          )}
        </View>
      </ScrollView>
      {/* 
      <View style={{ width: '100%', left: 0, right: 0 }}>
        <TouchableWithoutFeedback
          style={{
            width: '100%',
            backgroundColor: '#3888C6',
            justifyContent: 'center',
            alignItems: 'center',
            alignContent: 'center',
            height: 80,
          }}
          onPress={() => {
            if (clickable === true) {
              setclickable(false);

              dispatch(
                EditDeliveryAction(
                  name,
                  phone,
                  address,
                  city,
                  imagebaseprofile,
                  cityid,
                  token
                )
              );
            } else {
              setclickable(true);
            }
          }}>
          <View
            style={{
              backgroundColor: '#3888C6',
              alignItems: 'center',
            }}>
            {clickable === false && (
              <Text
                style={{
                  color: 'white',
                  fontSize: 20,
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignContent: 'center',
                  padding: 15,
                  textAlign: 'center',
                  marginBottom: 20,
                }}>
                {editproduct}{' '}
              </Text>
            )}
            {clickable === true && (
              <Text
                style={{
                  color: 'white',
                  fontSize: 20,
                  justifyContent: 'center',
                  alignItems: 'center',
                  alignContent: 'center',
                  padding: 15,
                  textAlign: 'center',
                  marginBottom: 20,
                }}>
                {saveproduct}{' '}
              </Text>
            )}
          </View>
        </TouchableWithoutFeedback>
      </View>
   
    */}
    </ImageBackground>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'yellow',
  },
  image: {
    width: 150,
    height: 150,
    borderRadius: 80,
    marginRight: 10,
    marginLeft: 10,
  },
  socialContainer: {
    marginLeft: 30,
    marginRight: 30,
    marginTop: 30,
    width: DEVICE_width - 60,
    height: 40,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },

  image: {
    width: 150,
    height: 150,
    marginBottom: 20,
  },
  text: {
    flex: 1,
    justifyContent: 'flex-start',
    flexDirection: 'row',
    color: 'white',
    backgroundColor: 'transparent',
    marginTop: 5,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },

  text: {
    color: 'white',
    fontWeight: 'bold',
    backgroundColor: 'transparent',
    marginTop: 30,
    marginBottom: 20,
  },
  btnEye: {
    position: 'absolute',
    top: 10,
    left: 40,
  },
  iconEye: {
    width: 25,
    height: 25,
    tintColor: '#3888C6',
  },
  btnEyeRight: {
    position: 'absolute',
    top: 10,
    right: 40,
  },
  iconEyeRight: {
    width: 25,
    height: 25,
    tintColor: '#3888C6',
  },
  buttonContainer: {
    height: 45,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    width: 250,
    borderRadius: 5,
  },
  loginButton: {
    backgroundColor: '#3888C6',
  },
  restoreButtonContainer: {
    width: 250,
    marginBottom: 15,
    alignItems: 'flex-end',
  },
  socialButtonContent: {
    marginRight: 20,
    marginLeft: 20,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  socialIcon: {
    color: '#FFFFFF',
    marginRight: 5,
  },
  socialLoginButton: {
    backgroundColor: '#929292',
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 10,
    borderRadius: 5,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  loginText: {
    color: '#FFFFFF',
    alignItems: 'center',
    alignContent: 'center',
    alignSelf: 'center',
    justifyContent: 'center',
  },
  icon: {
    width: 30,
    height: 30,
  },
  buttonText: {
    fontSize: 20,
    textAlign: 'center',
    color: 'white',
    paddingHorizontal: 20,
  },
  buttonStyle: {
    borderRadius: 5,
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 30,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  input: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
    borderColor: 'grey',
    color: 'grey',
  },
  inputaddress: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 60,
    height: 70,
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
    borderColor: 'grey',
    color: 'grey',
  },
  textaddress: {
    fontSize: 20,
    backgroundColor: '#ffffff',
    width: DEVICE_width,
    textAlign: 'right',
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',

    flexDirection: 'row',
    color: 'grey',
  },

  inputtt: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 60,
    height: 50,
    flexDirection: 'row-reverse',
    marginBottom: 20,

    alignItems: 'center',
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 5,
    borderColor: 'grey',
    color: 'grey',
  },
  inputDialog: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 100,
    height: 40,
    marginTop: 15,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    paddingRight: 20,
    textAlign: 'right',
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
    borderColor: 'grey',
    color: 'grey',
  },
  inputRight: {
    backgroundColor: '#ffffff',
    width: DEVICE_width - 60,
    height: 40,
    marginHorizontal: 30,
    paddingRight: 20,
    paddingTop: 5,
    paddingLeft: 20,
    textAlign: 'left',
    borderWidth: 1,
    borderRadius: 5,
    flexDirection: 'row',
    borderColor: 'grey',
    color: 'grey',
  },
  inputWrapper: {
    flexDirection: 'row-reverse',
    marginBottom: 20,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  inputWrapperr: {
    flexDirection: 'row-reverse',
    width: DEVICE_width / 2,
    marginBottom: 20,
    justifyContent: 'center',
    alignContent: 'center',
    alignItems: 'center',
  },
  cover: {
    height: '90%',
  },
  sheet: {
    position: 'absolute',
    top: Dimensions.get('window').height,
    left: 0,
    right: 0,
    height: '100%',
    justifyContent: 'flex-end',
  },
  popup: {
    backgroundColor: '#FFF',
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    alignItems: 'center',
    justifyContent: 'center',
    minHeight: 80,
  },
});
