import React, { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  View,
  Image,
  StyleSheet,
  Text,
  NativeModules,
  Platform,
  SafeAreaView,
  TouchableOpacity,
} from 'react-native';
import logoImg from '../imgs/logoo.gif';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { LoginUserAction } from '../actions/Actions';

import { GetCountriesCitiesCounranciesAction } from '../actions/Actions';
import FastImage from 'react-native-fast-image';

export default function Splash(props) {
  const dispatch = useDispatch();

  const [loggedin, setloggedin] = useState(null);
  const [first, setfirst] = useState(null);

  const loadApp = async () => {
    try {
      let loggedinh = await AsyncStorage.getItem('loggedin');
      setfirst(await AsyncStorage.getItem('first'));

      setloggedin(loggedinh);
    } catch (e) {}
  };

  useEffect(() => {
    loadApp();
    dispatch(GetCountriesCitiesCounranciesAction());

    timeoutHandle = setTimeout(() => {
      if (loggedin === 'true') {
        props.navigation.navigate('LoadingStack', {});
      } else if (loggedin === 'false' || first === null) {
        props.navigation.navigate('LoginStack', {});
      }
    }, 3000);
  }, [loggedin]);
  return (
    <View
      style={{
        backgroundColor: 'white',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      {/* <Image
          source={logoImg}
          resizeMode="cover"
          style={{
            alignSelf: 'center',
            height: '100%',
            width: '100%',
          }}
        /> */}
      <FastImage
        style={{
          alignSelf: 'center',
          height: '100%',
          width: '100%',
        }}
        source={logoImg}
        resizeMode={FastImage.resizeMode.cover}
        Priority={FastImage.priority.high}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
